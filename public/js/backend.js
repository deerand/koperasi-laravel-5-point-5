$(document).ready(function () {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $('.testClassWoi').DataTable();
  let i = 1;
  $(".addFile").click(function () {
    i++;
    $('.increment').append(
      '<div class="clone hide" id="row' + i + '">' +
      '<div class="control-group input-group" style="margin-top:10px">' +
      '<input type="file" name="nama[]" class="form-control" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,image/*">' +
      '<div class="input-group-btn">' +
      '<button class="btn btn-danger" id="' + i + '" type="button"><i class="la la-minus-square"></i></button>' +
      '</div>' +
      '</div>' +
      '</div>'
    );
  });
  $("body").on("click", ".btn-danger", function () {
    const button_id = $(this).attr("id");
    $('#row' + button_id + '').remove();
    i--;
  });
  $('select[name=provinsi_id]').change(function (e) {
    const provinsi_id = $(this).val();
    $.ajax({
      url: '/getKota/' + provinsi_id,
      method: 'GET',
      success: function (data) {
        let kabupaten_id = '<option value=>- Pilih Kabupaten -</option>';
        $.map(data, function (value, i) {
          kabupaten_id += `<option value=${value.id}>${value.name}</option>`;
        });
        $('select[name=kabupaten_id]').html(kabupaten_id);
        $('select[name=kecamatan_id]').html('');
        $('select[name=kelurahan_id]').html('');
      }
    });
  });
  $('select[name=kabupaten_id]').change(function (e) {
    const kabupaten_id = $(this).val();
    $.ajax({
      url: '/getKecamatan/' + kabupaten_id,
      method: 'GET',
      success: function (data) {
        let kecamatan_id = '<option value=>- Pilih Kecamatan -</option>';
        $.map(data, function (value, i) {
          kecamatan_id += `<option value=${value.id}>${value.name}</option>`;
        });
        $('select[name=kecamatan_id]').html(kecamatan_id);
        $('select[name=kelurahan_id]').html('');
      }
    });
  });
  $('select[name=kecamatan_id]').change(function (e) {
    const kecamatan_id = $(this).val();
    $.ajax({
      url: '/getKelurahan/' + kecamatan_id,
      method: 'GET',
      success: function (data) {
        let kelurahan_id = '<option value=>- Pilih Kelurahan -</option>';
        $.map(data, function (value, i) {
          kelurahan_id += `<option value=${value.id}>${value.name}</option>`;
        })
        $('select[name=kelurahan_id]').html(kelurahan_id);
      }
    })
  })
  var KTDatatablesDataSourceAjaxServer = function () {

    var initTable1 = function () {
      var table = $('#all-cop');

      // begin first table
      table.DataTable({
        responsive: true,
        searchDelay: 500,
        processing: true,
        serverSide: true,
        ajax: '/getAllKoperasi',
        columns: [
          { data: 'nama_institusi', name: 'nama_institusi' },
          { data: 'jenis.nama', name: 'jenis.nama' },
          { data: 'provinsi.name', name: 'provinsi.name' },
          { data: 'kabupaten.name', name: 'kabupaten.name' },
          { data: 'jml_anggota', name: 'jml_anggota' },
          { data: 'tgl_spk', name: 'tgl_spk' },
          { data: 'action', name: 'action', orderable: false, searchable: false }
        ],
      });
    };

    return {

      //main function to initiate the module
      init: function () {
        initTable1();
      },

    };

  }();

  jQuery(document).ready(function () {
    KTDatatablesDataSourceAjaxServer.init();
  });  //Bentuk Usaha
  const chartBentukUsaha = $('#bentuk_usaha');

  const bentukUsahaChart = new Chart(chartBentukUsaha, {
    type: "pie",
    data: {
      datasets: [{
        data: [0],
        backgroundColor: [
          mApp.getColor("info"),
          mApp.getColor("success"),
          mApp.getColor("danger"),
          mApp.getColor("warning")
        ],
        borderWidth: 1
      }],
      labels: ['Total'],
    },
    options: {
      title: {
        display: !1
      },
      tooltips: {
        intersect: !1,
        mode: "nearest",
        xPadding: 10,
        yPadding: 10,
        caretPadding: 10
      },
      legend: {
        display: !1
      },
      responsive: !0,
      maintainAspectRatio: !1,
      barRadius: 4,
      scales: {
        xAxes: [{
          display: !1,
          gridLines: !1,
          stacked: !0
        }],
        yAxes: [{
          display: !1,
          stacked: !0,
          gridLines: !1
        }]
      },
      layout: {
        padding: {
          left: 0,
          right: 0,
          top: 0,
          bottom: 0
        }
      }
    }
  })
  $.ajax({
    url: '/getBentukUsaha',
    method: 'GET',
    success: function (data) {
      const arrayBentuk = [];
      $.map(data, function (value, index) {
        arrayBentuk.push(value.total_bentuk_usaha_koperasi);
      });
      bentukUsahaChart.data.datasets.map(function (o) {
        o.data = arrayBentuk;
        return o;
      })
      bentukUsahaChart.data.labels = ['Tidak Ada', 'Primer Nasional', 'Primer Provinsi', 'Primer Kab atau Kota', 'Sekunder Nasional', 'Sekunder Provinsi', 'Sekunder Kab atau Kota'];
      bentukUsahaChart.update();
    }
  });
  //END BENTUK USAHA
  //KABUPATEN
  const chartKabupaten = $('#jumlah_tiap_kabupaten');
  const tiapKabupatenChart = new Chart(chartKabupaten, {
    type: "bar",
    data: {
      datasets: [{
        data: [0],
        backgroundColor: [
          mApp.getColor("info"),
          mApp.getColor("success"),
          mApp.getColor("danger"),
          mApp.getColor("warning"),
          mApp.getColor("secondary")
        ],
        borderWidth: 1
      }],
      labels: ['Total'],
    },
    options: {
      title: {
        display: !1
      },
      tooltips: {
        intersect: !1,
        mode: "nearest",
        xPadding: 10,
        yPadding: 10,
        caretPadding: 10
      },
      legend: {
        display: !1
      },
      responsive: !0,
      maintainAspectRatio: !1,
      barRadius: 4,
      scales: {
        xAxes: [{
          display: !1,
          gridLines: !1,
          stacked: !0
        }],
        yAxes: [{
          display: !1,
          stacked: !0,
          gridLines: !1
        }]
      },
      layout: {
        padding: {
          left: 0,
          right: 0,
          top: 0,
          bottom: 0
        }
      }
    }
  })
  $.ajax({
    url: '/getKabupaten',
    method: 'GET',
    success: function (data) {
      const arrayValueKabupaten = [];
      const arrayLabelsKabupaten = [];
      $.map(data, function (value, index) {
        arrayValueKabupaten.push(value.total_kabupaten_koperasi);
      });
      tiapKabupatenChart.data.datasets.map(function (o) {
        o.data = arrayValueKabupaten;
        return o;
      })

      $.map(data, function (value, index) {
        arrayLabelsKabupaten.push(value.nama_kabupaten);
      })
      tiapKabupatenChart.data.labels = arrayLabelsKabupaten;
      tiapKabupatenChart.update();
    }
  });
  //END KABUPATEN
  //JUMLAH ANGGOTA
  const chartJumlahAnggota = $('#jumlah_anggota_tiap_koperasi');
  const jumlahAnggotaTiapKoperasiChart = new Chart(chartJumlahAnggota, {
    type: "bar",
    data: {
      datasets: [{
        data: [0],
        backgroundColor: [
          mApp.getColor("info"),
          mApp.getColor("success"),
          mApp.getColor("danger"),
          mApp.getColor("warning"),
          mApp.getColor("secondary")
        ],
        borderWidth: 1
      }],
      labels: ['Total'],
    },
    options: {
      title: {
        display: !1
      },
      tooltips: {
        intersect: !1,
        mode: "nearest",
        xPadding: 10,
        yPadding: 10,
        caretPadding: 10
      },
      legend: {
        display: !1
      },
      responsive: !0,
      maintainAspectRatio: !1,
      barRadius: 4,
      scales: {
        xAxes: [{
          display: !1,
          gridLines: !1,
          stacked: !0
        }],
        yAxes: [{
          display: !1,
          stacked: !0,
          gridLines: !1
        }]
      },
      layout: {
        padding: {
          left: 0,
          right: 0,
          top: 0,
          bottom: 0
        }
      }
    }
  })
  $.ajax({
    url: '/getJmlAnggotaKoperasi',
    method: 'GET',
    success: function (data) {
      const arrayValueJumlahAnggota = [];
      const arrayLabelsJumlahAnggota = [];
      $.map(data, function (value, index) {
        arrayValueJumlahAnggota.push(value.jml_anggota);
      });
      jumlahAnggotaTiapKoperasiChart.data.datasets.map(function (o) {
        o.data = arrayValueJumlahAnggota;
        return o;
      })

      $.map(data, function (value, index) {
        arrayLabelsJumlahAnggota.push(value.nama_institusi);
      })
      jumlahAnggotaTiapKoperasiChart.data.labels = arrayLabelsJumlahAnggota;
      jumlahAnggotaTiapKoperasiChart.update();
    }
  });
  //END JUMLAH ANGGOTA
});