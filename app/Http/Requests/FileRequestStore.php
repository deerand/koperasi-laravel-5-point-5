<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FileRequestStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama.*' => 'required|file|mimes:xlsx,xls,csv,jpg,jpeg,png,bmp,doc,docx,pdf'
        ];
    }
    public function messages() {
        return [
            'nama.*.required' => ':attribute Harus di Isi',
            'nama.*.file' => ':attribute Harus Bertipe File',
            'nama.*.mimes' => ':attribute Harus Berekstensi (Excel, Gambar, Word, .CSV, PDF)'
        ];
    }
    public function attributes() {
        return [
            'nama.*' => 'File'
        ];
    }
}
