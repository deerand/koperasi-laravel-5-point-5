<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class DashboardController extends Controller
{
    public function index()
    {
        return view('admin.dashboard.index');
    }
    public function getBentukUsaha()
    {
        $koperasi = DB::table('koperasi')
                    ->select(DB::raw('count(*) as total_bentuk_usaha_koperasi'))                    
                    ->groupBy('bentuk_usaha_id')
                    ->get();

        return response($koperasi,200);
    }
    public function getKabupaten()
    {
        $koperasi = DB::table('koperasi')
                    ->select(DB::raw('count(*) as total_kabupaten_koperasi, kabupaten.name as nama_kabupaten'))
                    ->join('kabupaten','kabupaten.id', '=', 'koperasi.kabupaten_id')
                    ->groupBy('kabupaten_id')
                    ->get();

        return response($koperasi,200);
    }
    public function getJmlAnggotaKoperasi()
    {
        $koperasi = DB::table('koperasi')
                    ->select(['jml_anggota','nama_institusi'])
                    ->get();
                    
        return response($koperasi,200);
    }
}
