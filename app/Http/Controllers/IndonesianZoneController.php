<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KotaModel;
use App\KecamatanModel;
use App\KelurahanModel;

class IndonesianZoneController extends Controller
{
    public function getKota($id)
    {
        $kota = KotaModel::where('provinsi_id',$id)->get();

        return response($kota,200);
    }
    public function getKecamatan($id)
    {
        $kecamatan = KecamatanModel::where('kabupaten_id',$id)->get();

        return response($kecamatan,200);
    }
    public function getKelurahan($id)
    {
        $kelurahan = KelurahanModel::where('kecamatan_id',$id)->get();

        return response($kelurahan,200);
    }
}
