<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\User;
use Auth;
use Hash;
class ManagementUserController extends Controller
{
    public function formRole()
    {
        return view('admin.user.formRole');
    }
    public function addRole(Request $request) {

        try {
            $role = Role::create(['name' => $request->input('name')]);        
            return back()->with('success', 'Data Berhasil di Tambahkan');
        } catch (\Exception $e) {
            return back()->with('danger', $e->getMessage());
        }
    }
    public function allUsers()
    {        
        $user = User::where('id', '!=', Auth::user()->id)->with('roles')->get();

        return view('admin.user.index',['user' => $user]);
    }
    public function createUser()
    {
        $role = Role::all();
        return view('admin.user.create',['role' => $role]);
    }
    public function storeUser(Request $request)
    {
        try {
            $user = User::create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => Hash::make($request->input('password'))
            ]);
            $user->assignRole($request->input('role'));
            return back()->with('success','Data Berhasil di Masukan');
        } catch (\Exception $e) {
            return back()->with('danger', $e->getMessage());
        }
    }
    public function editUser($id)
    {
        $user = User::with('roles')->findOrFail($id);        
        $role = Role::all();        
        return view('admin.user.edit',['user' => $user, 'role' => $role]);
    }
    public function updateUser(Request $request)
    {
        try {
            $user = User::findOrFail($request->input('id'));        
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->save();

            return back()->with('success','Data Berhasil di Update');
        } catch (\Exception $e) {
            return back()->with('danger', $e->getMessage());
        }
    }
    public function destroyUser($id)
    {
        try {
            $user = User::findOrFail($id);
            $user->delete();

            return back()->with('sucess','Data Berhasil di Hapus');
        } catch (\Exception $e) {
            return back()->with('danger', $e->getMessage());
        }
    }
}
