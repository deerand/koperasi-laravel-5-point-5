<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KoperasiModel;
use App\JenisKoperasiModel;
use App\BentukUsahaModel;
use App\JenisUsahaModel;
use App\KelompokUsahaModel;
use App\SektorUsahaModel;
use App\RekapFileModel;
use App\KotaModel;
use App\KecamatanModel;
use App\KelurahanModel;
use Auth;
use File;
use Carbon\Carbon;
use App\ProvinsiModel;
use Terbilang;
use DataTables;
use DB;
use App\Exports\KoperasiExport;
use Maatwebsite\Excel\Facades\Excel;

class KoperasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function uniqueFilename($path, $name, $ext) {
	
        $output = $name;
        $basename = basename($name, '.' . $ext);
        $i = 2;
        
        while(File::exists($path . '/' . $output)) {
            $output = $basename . $i . '.' . $ext;
            $i ++;
        }
        
        return $output;
        
    }
    public function index()
    {
        $jenisKoperasi = JenisKoperasiModel::all();
        return view('admin.koperasi.index',['jenisKoperasi' => $jenisKoperasi]);
    }
    public function allKoperasi()
    {
        // $koperasi = KoperasiModel::with(['jenis','provinsi','kabupaten']);
        // return view('admin.koperasi.allKoperasi',['koperasi' => $koperasi]);
        return view('admin.koperasi.allKoperasi');
    }
    public function getAllKoperasi() {
        $koperasi = KoperasiModel::with(['provinsi','kabupaten','jenis'])->orderBy('created_at','ASC');
        return Datatables::of($koperasi)
        ->addColumn('action',function($item){
            return view('admin.koperasi.buttonAllKoperasi',['item' => $item])->render();
        })
        ->make(true);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $jenisKoperasi = JenisKoperasiModel::all();
        $bentukUsahaModel = BentukUsahaModel::all();
        $jenisUsahaModel = JenisUsahaModel::all();
        $kelompokUsahaModel = KelompokUsahaModel::all();
        $sektorUsahaModel = SektorUsahaModel::all();
        $provinsi = ProvinsiModel::all();
        return view('admin.koperasi.create',[
            'jenisKoperasi' => $jenisKoperasi,
            'bentukUsahaModel' => $bentukUsahaModel,
            'jenisUsahaModel' => $jenisUsahaModel,
            'kelompokUsahaModel' => $kelompokUsahaModel,
            'sektorUsahaModel' => $sektorUsahaModel,
            'provinsi' => $provinsi
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {            
            $koperasi = new KoperasiModel;
            $koperasi->jenis_id = $request->input('jenis_id');
            $koperasi->users_id = Auth::user()->id;
            $koperasi->sektor_usaha_id = $request->input('sektor_usaha_id');
            $koperasi->bentuk_usaha_id = $request->input('bentuk_usaha_id');
            $koperasi->kelompok_usaha_id = $request->input('kelompok_usaha_id');
            $koperasi->jenis_usaha_id = $request->input('jenis_usaha_id');
            $koperasi->provinsi_id = $request->input('provinsi_id');
            $koperasi->kabupaten_id = $request->input('kabupaten_id');
            $koperasi->kecamatan_id = $request->input('kecamatan_id');
            $koperasi->kelurahan_id = $request->input('kelurahan_id');
            $koperasi->nomor_spk = $request->input('nomor_spk');
            $koperasi->tgl_spk = $request->input('tgl_spk');
            $koperasi->nama_institusi = $request->input('nama_institusi');
            $koperasi->nomor_badan_hukum_awal = $request->input('nomor_badan_hukum_awal');
            $koperasi->nomor_badan_hukum_akhir = $request->input('nomor_badan_hukum_akhir');
            $koperasi->nama_ketua = $request->input('nama_ketua');
            $koperasi->alamat = $request->input('alamat');
            $koperasi->jml_anggota = $request->input('jml_anggota');            
            $koperasi->biaya_hosting = $request->input('biaya_hosting');
            $koperasi->pic_teknik = $request->input('pic_teknik');
            $koperasi->no_telp = $request->input('no_telp');
            $koperasi->email = $request->input('email');
            $koperasi->url = $request->input('url');
            $koperasi->syariah = $request->input('syariah');
            $koperasi->level = $request->input('level');
            $koperasi->status = $request->input('status');
            $koperasi->tgl_setup = $request->input('tgl_setup');
            $koperasi->harga_aplikasi = $request->input('harga_aplikasi');
            $koperasi->history = $request->input('history');
            $koperasi->status_pembayaran = $request->input('status_pembayaran');
            $koperasi->save();
            
            return redirect('koperasi')->with('success','Data Berhasil di Masukan');
        } catch (\Exception $e) {
            return back()->with('danger', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jenisKoperasi = JenisKoperasiModel::with(['koperasi','users'])->findOrFail($id);        
        return view('admin.koperasi.show',['jenisKoperasi' => $jenisKoperasi]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $provinsi = ProvinsiModel::all();
        $bentukUsahaModel = BentukUsahaModel::all();
        $jenisUsahaModel = JenisUsahaModel::all();
        $kelompokUsahaModel = KelompokUsahaModel::all();
        $sektorUsahaModel = SektorUsahaModel::all();
        $koperasi = KoperasiModel::with(['jenis','rekap_file','bentukUsaha','jenisUsaha','kelompokUsaha','sektorUsaha','provinsi','kabupaten','kecamatan','kelurahan'])->findOrFail($id);
        $kabupaten = KotaModel::where('provinsi_id', $koperasi->provinsi->id)->get();        
        $kecamatan = KecamatanModel::where('kabupaten_id', $koperasi->kabupaten->id)->get();        
        if($koperasi->kecamatan == null)
        {
            $kelurahan = KelurahanModel::all();
        } else {
            $kelurahan = KelurahanModel::where('kecamatan_id', $koperasi->kecamatan->id)->get();
        }
        $jenis = JenisKoperasiModel::all();

        return view('admin.koperasi.edit',[
            'provinsi' => $provinsi,
            'kabupaten' => $kabupaten,
            'kecamatan' => $kecamatan,
            'kelurahan' => $kelurahan,
            'koperasi' => $koperasi,
            'jenis' => $jenis,
            'bentukUsahaModel' => $bentukUsahaModel,
            'jenisUsahaModel' => $jenisUsahaModel,
            'kelompokUsahaModel' => $kelompokUsahaModel,
            'sektorUsahaModel'=> $sektorUsahaModel
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        
        try {
            $koperasi = KoperasiModel::with('jenis')->findOrFail($id);
            $koperasi->jenis_id = $request->input('jenis_id');
            $koperasi->users_id = Auth::user()->id;
            $koperasi->sektor_usaha_id = $request->input('sektor_usaha_id');
            $koperasi->bentuk_usaha_id = $request->input('bentuk_usaha_id');
            $koperasi->kelompok_usaha_id = $request->input('kelompok_usaha_id');
            $koperasi->jenis_usaha_id = $request->input('jenis_usaha_id');
            $koperasi->provinsi_id = $request->input('provinsi_id');
            $koperasi->kabupaten_id = $request->input('kabupaten_id');
            $koperasi->kecamatan_id = $request->input('kecamatan_id');
            $koperasi->kelurahan_id = $request->input('kelurahan_id');
            $koperasi->nomor_spk = $request->input('nomor_spk');
            $koperasi->tgl_spk = $request->input('tgl_spk');
            $koperasi->nama_institusi = $request->input('nama_institusi');
            $koperasi->nama_ketua = $request->input('nama_ketua');
            $koperasi->alamat = $request->input('alamat');
            $koperasi->jml_anggota = $request->input('jml_anggota');
            $koperasi->biaya_hosting = $request->input('biaya_hosting');            
            $koperasi->pic_teknik = $request->input('pic_teknik');    
            $koperasi->no_telp = $request->input('no_telp');
            $koperasi->email = $request->input('email');
            $koperasi->url = $request->input('url');
            $koperasi->syariah = $request->input('syariah');
            $koperasi->level = $request->input('level');
            $koperasi->status = $request->input('status');            
            $koperasi->tgl_setup = $request->input('tgl_setup');
            $koperasi->harga_aplikasi = $request->input('harga_aplikasi');
            $koperasi->history = $request->input('history');
            $koperasi->status_pembayaran = $request->input('status_pembayaran');

            $koperasi->save();

            return redirect('koperasi/'.$koperasi->jenis->id)->with('success', 'Data Berhasil di Perbaharui');         
        } catch (\Exception $e) {
            return back()->with('danger', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $koperasi = KoperasiModel::with('rekap_file')->findOrFail($id);
            foreach($koperasi->rekap_file as $value)
            {
                $path = public_path()."/files/".$value->nama;
                File::delete($path);
            }            
            $koperasi->delete();

            return back()->with('success','Data Berhasil di Hapus');
        } catch (\Exception $e) {
            return back()->with('danger', $e->getMessage());
        }
    }
public function getRomawi($bln){
        switch ($bln){
            case 1:
                $array = ['I', 'Januari'];
                return $array;
                break;
            case 2:
                $array = ['II', 'Februari'];
                return $array;
                break;
            case 3:
                $array = ['III', 'Maret'];
                return $array;
                break;
            case 4:
                $array = ['IV', 'April'];
                return $array;
                break;
            case 5:
                $array = ['V', 'Mei'];
                return $array;
                break;
            case 6:
                $array = ['VI', 'Juni'];
                return $array;
            case 7:
                $array = ['VII', 'Juli'];
                return $array;
                break;
            case 8:
                $array = ['VIII', 'Agustus'];
                return $array;
                break;
            case 9:
                $array = ['IX', 'September'];
                return $array;
            case 10:
                $array = ['X', 'Oktober'];
                return $array;
                break;
            case 11:
                $array = ['XI', 'November'];
                return $array;
                break;
            case 12:
                $array = ['XII', 'Desember'];
                return $array;
                break;
        }
    }
    public function exportWord($id)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $koperasi = KoperasiModel::find($id);
        $section = $phpWord->addSection();
        // Create a new table style
        $tableStyleTop = new \PhpOffice\PhpWord\Style\Table;            
        $tableStyleTop->setUnit(\PhpOffice\PhpWord\Style\Table::WIDTH_PERCENT);
        $tableStyleTop->setWidth(100 * 50);        

        
        $tableStyle = array(
            'cellMarginRight' => 100,
            'cellMarginTop' => 100,
            'cellMarginBottom' => 100,
            'cellMarginLeft' => 100,
            'borderSize' => 1,
            'unit' => \PhpOffice\PhpWord\Style\Table::WIDTH_PERCENT,
            'width' => 5000
        );
        $phpWord->addFontStyle('headerFontStyle', array('bold'=>true, 'size'=>14, 'name' => 'Times New Roman'));
        $phpWord->addParagraphStyle('headerParagraphStyle', array('align'=>'center', 'spaceAfter'=>50));
        $section->addTextBreak(1);
        $section->addText("BERITA ACARA SERAH TERIMA PEKERJAAN","headerFontStyle","headerParagraphStyle");
        $section->addText("Pembelian/Sewa Alat Penunjang / Aplikasi Smartcoop  pada Bimbingan Teknis Penguatan Usaha KSP/USP-Koperasi dan KSPPS/USPPS-Koperasi","headerFontStyle","headerParagraphStyle");
        $section->addText('', [], ['borderBottomSize' => 6]);
        $converSprinttf = sprintf("%'03d", $koperasi->id);
        $section->addText("No.".$converSprinttf."/BAST-4VM/SMARTCOOP/".$this->getRomawi(date('m'))[0]."/2019",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'center'));
        $section->addTextBreak(0.5);
        // Define table style arrays
        $styleTable = array('borderSize'=>2, 'cellMargin'=>10,'width' => 100);
        $styleFirstRow = array('borderBottomSize'=>5, 'bgColor'=>'#D3D3D3');
        // Define cell style arrays
        $styleCell = array('width'=>'5000');
        $styleCellBTLR = array('valign'=>'center');
        // Define font style for first row
        $fontStyle = array('bold'=>true, 'align'=>'center');


        // Add table style
        // $phpWord->addTableStyle('CoverLine', $styleTable, $styleFirstRow);
        $phpWord->addNumberingStyle(
            'multilevel',
            array(
                'type' => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360),
                    array('format' => 'upperLetter', 'text' => '%2.', 'left' => 720, 'hanging' => 360, 'tabPos' => 720),
                )
            )
        );
        // Add table
        $table = $section->addTable($tableStyleTop);
        $table->addRow(50);
        $table->addCell(2200)->addText('Nama Pekerjaan',array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'both'));
        $table->addCell(200)->addText(':',array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'both'));
        $table->addCell(4000)->addText('Pelatihan dan Bimbingan Teknik Penggunaan Aplikasi Digitalisasi Koperasi - Smartcoop',array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'both'));
        $table->addRow(50);
        $table->addCell(2200)->addText('Nama Koperasi',array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'both'));
        $table->addCell(200)->addText(':',array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'both'));
        $table->addCell(4000)->addText($koperasi->nama_institusi." - ".strtoupper ($koperasi->kabupaten->name),array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'both'));
        $table->addRow(50);
        $table->addCell(2200)->addText('Detail Pekerjaan',array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'both'));
        $table->addCell(200)->addText(':',array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'both'));
        $cell = $table->addCell(4000,$styleCellBTLR);
        $cell->addListItem("Pembelian/Sewa Alat Penunjang digitalisasi koperasi / Aplikasi Smartcoop dengan alamat aplikasi :  ".$koperasi->url, 0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel', array('
        spaceAfter' => 25));
        $cell->addListItem("Pelatihan Penggunaan Aplikasi", 0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel',array('spaceAfter' => 25));
        $cell->addListItem("Bimbingan Teknis Penggunaan Aplikasi ", 0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel',array('spaceAfter' => 50));

        $section->addText("Dengan ini kami dari pihak Four Vision Media sebagai perusahaan konsultan penjual aplikasi telah menyelesaikan pekerjaan (100%) sebagaimana tersebut diatas, dan pihak peserta kegiatan dalam hal ini ". $koperasi->nama_institusi.", telah menerima hasil pekerjaan sebagaimana tersebut diatas dengan baik dan sesuai",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'both','spaceAfter' => 25));
        $section->addText("Checklist Pekerjaan :",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'both'));

        $table = $section->addTable($tableStyle);
        $table->addRow(50);
        $table->addCell(10)->addText('NO',array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'center'));
        $table->addCell(1550)->addText('Detail Pekerjaan',array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'center'));
        $table->addCell(1000)->addText('Checklist',array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'center'));
        $table->addRow(50);
        $table->addCell(10)->addText('1',array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'center'));
        $table->addCell(1550)->addText("Aplikasi Tata Kelola Koperasi berbasis Online : ".$koperasi->url." dengan kondisi baik dan dapat di akses dengan sempurna.",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'both','spaceAfter' => 25));
        $table->addCell(1000)->addText("");
        $table->addRow(50);
        $table->addCell(10)->addText('2',array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'center'));
        $table->addCell(1550)->addText("Pelatihan dan Bimbingan Teknis Penggunaan Aplikasi oleh pihak perusahaan atau pihak Dinas Koperasi dan Usaha Kecil Prov Jawa Barat",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'both','spaceAfter' => 25));
        $table->addCell(1000)->addText("");
        
        $section->addText("Demikian Berita acara serah terima ini kami buat, untuk selanjutnya pihak perusahaan dan peserta kegiatan memberikan Checklist atas pekerjaan tersebut.",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'both'));
        $section->addText("Bandung,      ".$this->getRomawi(date('m'))[1]." 2019",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'center'));        
        $table3 = $section->addTable($tableStyleTop);
        $table3->addRow(50);
        $table3->addCell(2200)->addText("Yang Menyerahkan", array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'center'));
        $table3->addCell(200);
        $table3->addCell(2200)->addText("Penerima Hasil / Pekerjaan Peserta Kegiatan", array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'center'));
        $table3->addRow(50);
        $table3->addCell(2200);
        $table3->addCell(200);
        $table3->addCell(2200);
        $table3->addRow(50);
        $table3->addCell(2200);
        $table3->addCell(200);
        $table3->addCell(2200);
        $table3->addRow(50);
        $table3->addCell(2200)->addText("Muhammad Ihsan Firdaus",array('bold' => true, 'underline' => 'single', 'size'=>12, 'name' => 'Times New Roman'),array('align' => 'center', 'spaceAfter' => 0));
        $table3->addCell(200);
        $table3->addCell(2200)->addText("..............................................................",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'center', 'spaceAfter' => 0));
        $table3->addRow(50);
        $table3->addCell(2200)->addText("DIREKTUR", array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'center', 'spaceAfter' => 0));
        $table3->addCell(200);
        $table3->addCell(2200)->addText("$koperasi->nama_institusi", array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'center', 'spaceAfter' => 0));
        $table3->addRow(50);
        $table3->addCell(2200)->addText("Four Vision Media", array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'center', 'spaceAfter' => 0));
        $table3->addCell(200);
        $table3->addCell(2200)->addText("Ketua Koperasi", array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'center', 'spaceAfter' => 0));

        $objectWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        try {
            $objectWriter->save(public_path("/bap_word/BAST_".$koperasi->nama_institusi.".docx"));
        } catch (\Exception $e)
        {
            return back()->with('danger', $e->getMessage());
        }

        return response()->download(public_path("/bap_word/BAST_".$koperasi->nama_institusi.".docx"));
    }
    public function formSpk($id){
        $koperasi = KoperasiModel::find($id);

        return view('admin.koperasi.formSpk',['koperasi' => $koperasi]);

    }
    public function biayaHosting($totalAnggota)
    {        
        if($totalAnggota <= 1000)
        {
            $arrayGetBiaya = array('keterangan' => 'Untuk Anggota Kurang Dari 1000', 'biaya' => 150000);
            return $arrayGetBiaya;
        } elseif ($totalAnggota <= 2000)
        {
            $arrayGetBiaya = array('keterangan' => 'Untuk Anggota 1001 Dari 2000', 'biaya' => 175000);
            return $arrayGetBiaya;
        } elseif ($totalAnggota <= 3000)
        {
            $arrayGetBiaya = array('keterangan' => 'Untuk Anggota 2001 Dari 3001 ', 'biaya' => 225000);
            return $arrayGetBiaya;            
        } elseif ($totalAnggota <= 4000)
        {
            $arrayGetBiaya = array('keterangan' => 'Untuk Anggota 3001 Dari 4000 ', 'biaya' => 275000);
            return 275000;
        } elseif ($totalAnggota <= 5000)
        {
            $arrayGetBiaya = array('keterangan' => 'Untuk Anggota 4001 Dari 5000 ', 'biaya' => 325000);
            return $arrayGetBiaya;
        } else {
            $arrayGetBiaya = array('keterangan' => 'Untuk Anggota 4001 Dari 5000 ', 'biaya' => 475000);
            return $arrayGetBiaya;
        }
    }
    public function exportSpk(Request $request, $id)
    {

        $koperasi = KoperasiModel::with('jenis')->find($id);
        $jenisKoperasi = $koperasi->jenis->nama;

        $requstHargaAplikasi = $request->input('harga_aplikasi');

        $time = strtotime($koperasi->tgl_spk);

        $tableStyle = array(
            'cellMarginRight' => 100,
            'cellMarginTop' => 100,
            'cellMarginBottom' => 100,
            'cellMarginLeft' => 100,
            'borderSize' => 1,
            'unit' => \PhpOffice\PhpWord\Style\Table::WIDTH_PERCENT,
            'width' => 5000
        );

        $tableStylePihakKedua = new \PhpOffice\PhpWord\Style\Table;            
        $tableStylePihakKedua->setUnit(\PhpOffice\PhpWord\Style\Table::WIDTH_PERCENT);
        $tableStylePihakKedua->setWidth(100 * 50);

        $calBiayaHosting = $this->biayaHosting($koperasi->jml_anggota);
        $labelAnggota = $calBiayaHosting['keterangan'];
        $jmlAnggota = number_format($calBiayaHosting['biaya'], 0, ',', '.');
        $terbilangAnggota = ucwords(Terbilang::make($calBiayaHosting['biaya']));

        $hargaAplikasi = number_format($requstHargaAplikasi , 0, ',', '.');
        $terbilangHargaAplikasi = ucwords(Terbilang::make($requstHargaAplikasi, ' rupiah'));

        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        $section = $phpWord->addSection();
        $phpWord->addFontStyle('headerFontStyle', array('bold'=>true, 'size'=>14, 'name' => 'Times New Roman'));
        $phpWord->addFontStyle('contentFontStyle', array('bold'=>false, 'size'=>12, 'name' => 'Times New Roman'));
        $phpWord->addFontStyle('secondHeaderFontStyle', array('bold'=>true, 'size'=>12, 'name' => 'Times New Roman'));

        $phpWord->addParagraphStyle('headerParagraphStyle', array('align'=>'center', 'spaceAfter'=>50));
        $phpWord->addParagraphStyle('contentParagraphStyle', array('align'=>'both', 'spaceAfter'=>50));

        $phpWord->addNumberingStyle(
            'multilevel',
            array(
                'type' => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360, 'font' => 'Times New Roman', 'fontSize' => 24),
                    array('format' => 'upperLetter', 'text' => '%2.', 'left' => 720, 'hanging' => 360, 'tabPos' => 720,'font' => 'Times New Roman', 'fontSize' => 24),
                )
            )
        );

        $phpWord->addNumberingStyle(
            'multilevel2',
            array(
                'type' => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360, 'font' => 'Times New Roman', 'fontSize' => 24),
                    array('format' => 'upperLetter', 'text' => '%2.', 'left' => 720, 'hanging' => 360, 'tabPos' => 720, 'font' => 'Times New Roman', 'fontSize' => 24),
                )
            )
        );

        $phpWord->addNumberingStyle(
            'multilevel3',
            array(
                'type' => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360, 'font' => 'Times New Roman', 'fontSize' => 24),
                    array('format' => 'upperLetter', 'text' => '%2.', 'left' => 720, 'hanging' => 360, 'tabPos' => 720, 'font' => 'Times New Roman', 'fontSize' => 24),
                )
            )
        );

        $phpWord->addNumberingStyle(
            'multilevel4',
            array(
                'type' => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360, 'font' => 'Times New Roman', 'fontSize' => 24),
                    array('format' => 'lowerLetter', 'text' => '%2.', 'left' => 1080, 'hanging' => 360, 'tabPos' => 720,'font' => 'Times New Roman', 'fontSize' => 24),
                )
            )
        );

        $phpWord->addNumberingStyle(
            'multilevel5',
            array(
                'type' => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360, 'font' => 'Times New Roman', 'fontSize' => 24),
                    array('format' => 'upperLetter', 'text' => '%2.', 'left' => 720, 'hanging' => 360, 'tabPos' => 720, 'font' => 'Times New Roman','fontSize' => 24),
                )
            )
        );

        $phpWord->addNumberingStyle(
            'multilevel6',
            array(
                'type' => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360, 'font' => 'Times New Roman', 'fontSize' => 24),
                    array('format' => 'lowerLetter', 'text' => '%2.', 'left' => 1080, 'hanging' => 360, 'tabPos' => 720,'font' => 'Times New Roman', 'fontSize' => 24),
                )
            )
        );

        $phpWord->addNumberingStyle(
            'multilevel7',
            array(
                'type' => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360, 'font' => 'Times New Roman', 'fontSize' => 24),
                    array('format' => 'lowerLetter', 'text' => '%2.', 'left' => 1080, 'hanging' => 360, 'tabPos' => 720, 'font' => 'Times New Roman', 'fontSize' => 24),
                )
            )
        );

        $phpWord->addNumberingStyle(
            'multilevel8',
            array(
                'type' => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360, 'font' => 'Times New Roman', 'fontSize' => 24),
                    array('format' => 'lowerLetter', 'text' => '%2.', 'left' => 1080, 'hanging' => 360, 'tabPos' => 720,'font' => 'Times New Roman', 'fontSize' => 24),
                )
            )
        );

        $phpWord->addNumberingStyle(
            'multilevel9',
            array(
                'type' => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360, 'font' => 'Times New Roman', 'fontSize' => 24),
                    array('format' => 'lowerLetter', 'text' => '%2.', 'left' => 1080, 'hanging' => 360, 'tabPos' => 720, 'font' => 'Times New Roman', 'fontSize' => 24),
                )
            )
        );

        $phpWord->addNumberingStyle(
            'multilevel10',
            array(
                'type' => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360, 'font' => 'Times New Roman', 'fontSize' => 24),
                    array('format' => 'lowerLetter', 'text' => '%2.', 'left' => 1080, 'hanging' => 360, 'tabPos' => 720, 'font' => 'Times New Roman', 'fontSize' => 24),
                )
            )
        );

        $phpWord->addNumberingStyle(
            'multilevel11',
            array(
                'type' => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360,'font' => 'Times New Roman', 'fontSize' => 24),
                    array('format' => 'lowerLetter', 'text' => '%2.', 'left' => 1080, 'hanging' => 360, 'tabPos' => 720, 'font' => 'Times New Roman', 'fontSize' => 24),
                )
            )
        );

        $phpWord->addNumberingStyle(
            'multilevel12',
            array(
                'type' => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360, 'font' => 'Times New Roman', 'fontSize' => 24),
                    array('format' => 'lowerLetter', 'text' => '%2.', 'left' => 1080, 'hanging' => 360, 'tabPos' => 720, 'font' => 'Times New Roman', 'fontSize' => 24),
                )
            )
        );
        
        $phpWord->addNumberingStyle(
            'multilevel13',
            array(
                'type' => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360, 'font' => 'Times New Roman', 'fontSize' => 24),
                    array('format' => 'upperLetter', 'text' => '%2.', 'left' => 720, 'hanging' => 360, 'tabPos' => 720, 'font' => 'Times New Roman', 'fontSize' => 24),
                )
            )
        );
        $section->addTextBreak(1);

        $converSprinttf = sprintf("%'03d", $koperasi->id);
        $getRomawi = $this->getRomawi(date('m',$time))[0];
        $getNumberYear = date('Y',$time);
        $section->addText("SURAT PERJANJIAN KERJASAMA","headerFontStyle","headerParagraphStyle");
        $section->addText("Nomor : $converSprinttf/SPK-APPS/KOP/4VM/$getRomawi/$getNumberYear","contentFontStyle","headerParagraphStyle");
        $section->addText("TENTANG PEMBELIAN PRODUK APLIKASI TATA KELOLA KOPERASI SMARTCOOP","secondHeaderFontStyle","headerParagraphStyle");
        $section->addText("Antara","contentFontStyle","headerParagraphStyle");
        $section->addText($koperasi->nama_institusi,"secondHeaderFontStyle","headerParagraphStyle");
        $section->addText("Dengan","contentFontStyle","headerParagraphStyle");
        $section->addText("CV.FOUR VISION MEDIA","secondHeaderFontStyle","headerParagraphStyle");
        $section->addTextBreak(1);
        setlocale(LC_TIME, 'Indonesian');        
        $date = Carbon::now();
        $dayName = $date->formatLocalized('%A');
        $monthName = $date->formatLocalized('%B');        
        $terbilangTanggal = ucwords(Terbilang::make(date('d')));
        $terbilangYear = ucwords(Terbilang::make(date('Y')));

        $section->addText("Pada hari ini $dayName tanggal $terbilangTanggal bulan $monthName tahun $terbilangYear kami yang bertanda tangan dibawah ini : ","contentFontStyle","contentParagraphStyle");

        $section->addListItem("$koperasi->nama_ketua dalam hal ini bertindak untuk dan atas nama Ketua $koperasi->nama_institusi yang berkedudukan di $koperasi->alamat , yang selanjutnya disebut PIHAK PERTAMA",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel', array('
        spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Muhamad Ihsan Firdaus, Dalam hal ini bertindak untuk dan atas nama Direktur CV Four Vision Media, yang berkedudukan di Jl. Batu Indah I no. 6 Batununggal Bandung, Provinsi Jawa Barat, yang selanjutnya disebut PIHAK KEDUA.",0, array('name' => 'Times New Roman', 'size' => 12,), 'multilevel', array('
        spaceAfter' => 25, 'align' => 'both'));

        $section->addText("Dengan ini kedua belah pihak sepakat untuk mengadakan perjanjian kerjasama dalam suatu kegiatan Pembelian Lisensi Produk Software Aplikasi Koperasi Simpan Pinjam SMART COOP yang selanjutnya di dalam perjanjian ini disebut “Aplikasi” untuk $koperasi->nama_institusi, sesuai dengan syarat-syarat sebagaimana tercantum dalam pasal-pasal tersebut di bawah ini.","contentFontStyle","contentParagraphStyle");

        $section->addText("PASAL 1","secondHeaderFontStyle","headerParagraphStyle");
        $section->addText("BENTUK KERJASAMA","secondHeaderFontStyle","headerParagraphStyle");

        $section->addTextBreak(1);

        $section->addListItem("PIHAK PERTAMA membeli Aplikasi kepada PIHAK KEDUA dan PIHAK KEDUA menerima dan menyanggupi untuk kegiatan pembelian lisensi produk software aplikasi tata kelola koperasi tersebut.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel2', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Pembelian Aplikasi adalah pembelian lisensi produk yang mencakup semua manfaat aplikasi yang tertuang pada fitur-fitur aplikasi sesuai dengan kelengkapan fitur aplikasi yang terinformasikan diawal pada proposal maupun demo aplikasi. Lisensi ini bersifat selamanya (lifetime license) dan tidak terbatas waktu.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel2', array('spaceAfter' => 25, 'align' => 'both'));

        $section->addTextBreak(1);
        
        $section->addText("PASAL 2","secondHeaderFontStyle","headerParagraphStyle");
        $section->addText("RINCIAN DAN HARGA APLIKASI","secondHeaderFontStyle","headerParagraphStyle");

        $section->addTextBreak(1);

        $section->addText("Pekerjaan Pembelian Aplikasi sebagaimana tersebut pada Pasal 1, dengan rincian sebagai berikut","contentFontStyle","contentParagraphStyle");

        $section->addListItem("Harga produk Aplikasi Smart Coop, adalah Rp. $hargaAplikasi,- ($terbilangHargaAplikasi). Harga belum termasuk pajak.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel3', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Biaya sewa server hosting untuk aplikasi Koperasi adalah sebesar Rp. $jmlAnggota,- ($terbilangAnggota Rupiah) per bulan yang dibayarkan setiap setahun sekali atau setiap bulan.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel3', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Jika diperlukan adanya kunjungan dari tim PIHAK KEDUA ke tempat koperasi PIHAK PERTAMA, maka biaya transportasi, konsumsi dan akomodasi ditanggung pihak PERTAMA diluar biaya aplikasi.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel3', array('spaceAfter' => 25, 'align' => 'both'));

        $section->addTextBreak(1);

        $section->addText("Berikut rincian pembelian aplikasi nya : ","contentFontStyle","contentParagraphStyle");  

        $section->addTextBreak(1);

        $table = $section->addTable($tableStyle);
        $table->addRow(50);
        $table->addCell(100)->addText('No.',array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'center'));
        $table->addCell(1000)->addText('Spesifikasi Produk',array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'center'));
        $table->addCell(2200)->addText('Kuantitas',array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'center'));
        $table->addCell(2200)->addText("Harga Satuan (Rp)",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'center'));
        $table->addCell(2200)->addText("Jumlah (Rp)",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'center'));
        $table->addRow(50);
        $table->addCell(100)->addText('1', array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'center'));
        $table->addCell(1000)->addText("Aplikasi Smartcoop untuk $jenisKoperasi", array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'both'));
        $table->addCell(2200)->addText('1 Unit Software Aplikasi', array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'left'));
        $table->addCell(2200)->addText("Rp. $hargaAplikasi,- / unit license", array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'left'));
        $table->addCell(2200)->addText("Rp. $hargaAplikasi,-", array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'left'));
        $table->addRow(50);
        $table->addCell(100)->addText('2', array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'center'));
        $table->addCell(1000)->addText("Sewa Domain dan Server Hosting per-Tahun", array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'both'));
        $table->addCell(2200)->addText("Disk Space 2 GB Bandwidth 200 GB Domain: $koperasi->url", array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'left'));
        $table->addCell(2200)->addText("Rp. $jmlAnggota,- / bulan *$labelAnggota", array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'left'));
        $table->addCell(2200)->addText("", array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'left'));
        $table->addRow(50);
        $table->addCell(100)->addText('', array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'center'));
        $table->addCell(100, ['vMerge' => 'continue', 'gridSpan' => 3])->addText('TOTAL', array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'right'));    

        $table->addCell(100)->addText("Rp. $hargaAplikasi,-", array('bold' => true, 'size'=>12, 'name' => 'Times New Roman'),array('align' => 'center'));
        $table->addRow(50);
        $table->addCell(null, ['vMerge' => 'restart', 'gridSpan' => 5])->addText("$terbilangHargaAplikasi", array('size'=>12, 'name' => 'Times New Roman', 'bold' => true),array('align' => 'center'));

        $section->addTextBreak(1);
        
        $section->addText("PASAL 3","secondHeaderFontStyle","headerParagraphStyle");
        $section->addText("JANGKA WAKTU PENYERAHAN APLIKASI","secondHeaderFontStyle","headerParagraphStyle");
        
        $section->addTextBreak(1);

        $section->addListItem("PIHAK KEDUA berkewajiban untuk melaksanakan dan menyerahkan (berupa link Aplikasi) kepada PIHAK PERTAMA Aplikasi sebagaimana disebutkan pada pasal 1 dengan jangka waktu paling lama 5 (lima) hari kalender, sejak diterimanya pembayaran pembelian aplikasi.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel4', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Waktu penyerahan dapat diperpanjang apabila ada permintaan tertulis dari PIHAK KEDUA pada PIHAK PERTAMA dan dapat disetujui selama menggunakan alasan-alasan yang kuat dan dapat dipertanggungjawabkan, termasuk diantaranya adalah Force Majeure.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel4', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Perpanjangan waktu penyerahan Aplikasi yaitu maksimum 14  (Empat Belas) hari kerja",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel4', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Jika setelah masa perpanjangan waktu telah habis, dan Aplikasi belum dapat diserahkan oleh PIHAK KEDUA kepada PIHAK PERTAMA, maka PIHAK KEDUA akan memberikan potongan harga kepada PIHAK PERTAMA sebesar Sepuluh Persen (10) dari Total Biaya yang disebutkan pada Pasal 2 diatas.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel4', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Penyerahan Aplikasi ditandai dengan telah aktifnya domain default aplikasi : $koperasi->url dan dengan telah diberikannya username dan password admin untuk akses login ke aplikasi.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel4', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("PIHAK KEDUA akan membantu setup awal data aplikasi meliputi :",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel4', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Setup personalized domain (alamat aplikasi tersendiri)",1, array('name' => 'Times New Roman', 'size' => 12), 'multilevel4', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Import Data Anggota Koperasi", 1, array('name' => 'Times New Roman', 'size' => 12), 'multilevel4', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Setting Neraca dan Saldo Awal", 1, array('name' => 'Times New Roman', 'size' => 12), 'multilevel4', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Proses setup aplikasi sebagaimana tertulis pada point 3 diatas, dikerjakan paling lama 25 hari.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel4', array('spaceAfter' => 25, 'align' => 'both'));

        $section->addTextBreak(1);

        $section->addText("PASAL 4","secondHeaderFontStyle","headerParagraphStyle");
        $section->addText("CARA PEMBAYARAN","secondHeaderFontStyle","headerParagraphStyle");

        $section->addTextBreak(1);
        
        $section->addListItem("Pembayaran biaya pembelian Aplikasi dan Sewa Server adalah sebesar Rp. $hargaAplikasi,- ($terbilangHargaAplikasi) untuk selanjutnya dibayarkan kepada PIHAK KEDUA.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel5', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Pembayaran dilakukan melalui Bank MANDIRI no. Rekening 130-002-0010107 atas nama CV FOUR VISION MEDIA. Atau via Bank BCA no. Rekening 3371562930 atas nama Muhamad Ihsan Firdaus (Direktur).",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel5', array('spaceAfter' => 25, 'align' => 'both'));

        $section->addTextBreak(1);

        $section->addText("PASAL 5","secondHeaderFontStyle","headerParagraphStyle");
        $section->addText("GARANSI DAN JAMINAN PEMELIHARAAN","secondHeaderFontStyle","headerParagraphStyle");

        $section->addTextBreak(1);

        $section->addListItem("PIHAK KEDUA memberikan Garansi Produk Aplikasi meliputi :",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel6', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Perbaikan error dan bug pada aplikasi",1, array('name' => 'Times New Roman', 'size' => 12), 'multilevel6', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Pergantian produk software aplikasi dengan aplikasi baru jika permasalahan pada aplikasi sudah tidak bisa diperbaiki",1, array('name' => 'Times New Roman', 'size' => 12), 'multilevel6', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Garansi keamanan software aplikasi",1, array('name' => 'Times New Roman', 'size' => 12), 'multilevel6', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("PIHAK PERTAMA langsung mendapatkan jasa pendampingan user administrator selama satu tahun.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel6', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("PIHAK KEDUA menjamin keamanan Aplikasi (Security web System), dan akan melakukan perbaikan segera, paling lambat 1x24 jam, jika terjadi masalah pada keamanan website maupun bug atau error pada sistem aplikasi yang mengakibatkan aplikasi tidak berjalan normal. Dan diharuskan melaporkan kondisi ini kepada PIHAK PERTAMA.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel6', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("PIHAK PERTAMA akan dikenakan biaya sewa server hosting dan domain setiap tahunnya, sebesar :",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel6', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Jasa sewa server Hosting $jmlAnggota",1, array('name' => 'Times New Roman', 'size' => 12), 'multilevel6', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Masa expired sewa server hosting, adalah tanggal  5 November  setiap tahunnya.",1, array('name' => 'Times New Roman', 'size' => 12), 'multilevel6', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("PIHAK KEDUA akan mengirimkan tagihan/invoice satu bulan sebelum masa expired hosting dan domain.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel6', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Jika PIHAK PERTAMA memutuskan untuk tidak memperpanjang sewa server hosting, maka PIHAK PERTAMA berhak atas semua data pada aplikasi, dan PIHAK KEDUA berkewajiban memberikan database aplikasi dalam format yang dapat dibaca (SQL dan CSV / XLSX).",0, array('name' => 'Times New Roman', 'size' => 12), 
        'multilevel6', array('spaceAfter' => 25, 'align' => 'both'));
        
        $section->addTextBreak(1);

        $section->addText("PASAL 6","secondHeaderFontStyle","headerParagraphStyle");
        $section->addText("KOSTUMASI APLIKASI DAN PENGEMBANGAN FITUR","secondHeaderFontStyle","headerParagraphStyle");

        $section->addTextBreak(1);

        $section->addListItem("PIHAK KEDUA secara berkala akan melakukan update terhadap aplikasi koperasi selama diperlukan adanya penambahan fitur sesuai Undang-undang koperasi yang berlaku untuk perkembangan aplikasi koperasi tanpa mengenakan biaya kepada PIHAK PERTAMA ",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel7', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("PIHAK PERTAMA berhak memberikan masukan kepada PIHAK KEDUA untuk melakukan kostumasi pada aplikasi sesuai dengan aturan yang berlaku pada Undang-undang dan peraturan per-koperasian.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel7', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Permintaan update atau penambahan modul dan fitur baru diluar modul aplikasi yang disebutkan di proposal akan dikenakan biaya sewajarnya sesuai dengan lingkup pekerjaan pembuatan fitur nya.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel7', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Permintaan update dan penambahan modul dan fitur baru sesuai dengan Pasal 6 ayat 3 diatas dimana permintaan penambahan modul dan fitur baru tersebut merupakan ide orisinal dan kreatifitas PIHAK PERTAMA, dan di luar dari apa yang disebutkan pada Pada pasal 6 ayat 1, maka modul dan fitur tersebut sepenuhnya hak cipta dari PIHAK PERTAMA, dan PIHAK KEDUA dilarang untuk menyebarluaskan tanpa seizin PIHAK PERTAMA sebagaimana dijelaskan lebih rinci pada Pasal  11 (Informasi Rahasia).",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel7', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Untuk detail pekerjaan update dan Maintenance akan dibuatkan proposal dan surat kerjasama terpisah.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel7', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("PIHAK KEDUA tidak bertanggung jawab dan berlepas diri atas setiap kebijakan koperasi PIHAK PERTAMA, meliputi penentuan Jasa simpanan maupun pinjaman atau kebijakan lain yang tertera pada AD/ART Koperasi PIHAK PERTAMA.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel7', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("PIHAK KEDUA tidak akan dibebankan tanggung jawab apapun baik dari sisi norma maupun dari sisi agama terkait adanya Jasa / margin / Bunga pada transaksi koperasi..",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel7', array('spaceAfter' => 25, 'align' => 'both'));

        $section->addTextBreak(10);

        $section->addText("PASAL 7","secondHeaderFontStyle","headerParagraphStyle");
        $section->addText("KORESPONDENSI","secondHeaderFontStyle","headerParagraphStyle");

        $section->addTextBreak(1);

        $section->addListItem("Berikut ini adalah korespondensi Para Pihak yang terlibat di dalam Perjanjian ini :",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel8', array('spaceAfter' => 25, 'align' => 'both'));        
        $section->addText("PIHAK PERTAMA","secondHeaderFontStyle");

        $table2 = $section->addTable($tableStylePihakKedua);
        $table2->addRow(50);
        $table2->addCell(2200)->addText("Nama",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'left'));
        $table2->addCell(200)->addText(":",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'left'));
        $table2->addCell(4000)->addText("$koperasi->pic_teknik",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'left'));
        $table2->addRow(50);
        $table2->addCell(2200)->addText("Alamat",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'left'));
        $table2->addCell(200)->addText(":",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'left'));
        $table2->addCell(4000)->addText("$koperasi->alamat",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'left'));
        $table2->addRow(50);
        $table2->addCell(2200)->addText("E-Mail",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'left'));
        $table2->addCell(200)->addText(":",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'left'));
        $table2->addCell(4000)->addText("$koperasi->email",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'left'));

        $section->addText("PIHAK KEDUA","secondHeaderFontStyle");

        $table3 = $section->addTable($tableStylePihakKedua);
        $table3->addRow(50);
        $table3->addCell(2200)->addText("Nama",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'left'));
        $table3->addCell(200)->addText(":",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'left'));
        $table3->addCell(4000)->addText("Asri Siti Rahmi",array('bold' => true, 'size'=>12, 'name' => 'Times New Roman'),array('align' => 'left'));
        $table3->addRow(50);
        $table3->addCell(2200)->addText("Alamat",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'left'));
        $table3->addCell(200)->addText(":",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'left'));
        $table3->addCell(4000)->addText("Komplek BNI Jl. Swadharma No. 44 Batunggal, Bandung Kidul, Bandung",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'left'));
        $table3->addRow(50);
        $table3->addCell(2200)->addText("Nomor Telp",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'left'));
        $table3->addCell(200)->addText(":",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'left'));
        $table3->addCell(4000)->addText("+62 813 2025 9883",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'left'));
        $table3->addRow(50);
        $table3->addCell(2200)->addText("E-Mail",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'left'));
        $table3->addCell(200)->addText(":",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'left'));
        $table3->addCell(4000)->addText("finance@4visionmedia.com",array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'left'));

        $section->addListItem("Surat menyurat ke alamat tersebut dianggap telah diterima dengan ketentuan sebagai berikut:",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel8', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Pada hari yang sama jika diserahkan langsung yang dibuktikan degan tanda tangan penerima pada buku pengantar surat atau tanda terima lain yang diterbitkan oleh pengirim.",1, array('name' => 'Times New Roman', 'size' => 12), 'multilevel8', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Pada hari yang sama jika pemberitahuan tersebut dikirimkan melalui faksimili/email dengan hasil baik.",1, array('name' => 'Times New Roman', 'size' => 12), 'multilevel8', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Dalam hal terjadi perubahan alamat dari alamat tersebut di atas atau alamat terakhir yang tercatat pada Para Pihak, maka perubahan tersebut harus diberitahukan secara tertulis kepada pihak lain dalam Perjanjian ini selambat-lambatnya 5 (lima) hari kerja sebelum perubahan alamat dimaksud berlaku efektif. Jika perubahan tersebut tidak diberitahukan berdasarkan Perjanjian ini, maka korespondensi dianggap telah diberikan dengan semestinya dengan tata cara sebagaimana dimaksud dalam ayat 2 pasal ini.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel8', array('spaceAfter' => 25, 'align' => 'both'));

        $section->addTextBreak(1);

        $section->addText("PASAL 8","secondHeaderFontStyle","headerParagraphStyle");
        $section->addText("FORCE MAJEURE","secondHeaderFontStyle","headerParagraphStyle");
        
        $section->addTextBreak(1);

        $section->addListItem("Force Majeure adalah kejadian kejadian di luar kekuasaan PARA PIHAK yang mengakibatkan terhentinya atau tertundanya pelaksanaan perjanjian yang tidak dapat dituntut seperti : perang, hujan deras, banjir, petir, gempa bumi, taufan, kebakaran, ledakan, sabotase, kerusuhan, huru hara, pemogokan, perubahan peraturan  atau larangan pemerintah.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel9', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Setiap kejadian yang bersifat force majeure, harus segera diberitahukan kepada pihak lainnya, paling lambat 7 (tujuh) hari kalender setelah kejadian tersebut.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel9', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Jika karena Force Majeur pelaksanaan Perjanjian Kerjasama ini tertunda untuk sementara waktu, maka lamanya pekerjaan yang tertunda tersebut diperpanjang sejumlah hari yang sama dengan lama terhentinya pelaksanaan pekerjaan.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel9', array('spaceAfter' => 25, 'align' => 'both'));

        $section->addTextBreak(1);

        $section->addText("PASAL 9","secondHeaderFontStyle","headerParagraphStyle");
        $section->addText("PERSELISIHAN","secondHeaderFontStyle","headerParagraphStyle");
        
        $section->addTextBreak(1);

        $section->addListItem("Bila terjadi perselisihan antara PIHAK PERTAMA dan PIHAK KEDUA, maka kedua belah pihak menyelesaikan perselisihan di Indonesia dengan cara musyawarah.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel10', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Jika tidak dapat diselesaikan secara musyawarah maka penyelesaian perselisihan dapat dilakukan melalui: (mediasi/konsiliasi/arbitrase/melalui pengadilan yang disepakati kedua belah pihak yaitu Pengadilan Negeri Bandung.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel10', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Segala biaya yang ditimbulkan akibat terjadinya perselisihan sebagaimana ayat (2) di atas, ditanggung oleh PARA PIHAK.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel10', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Proses penyelesaian sebagaimana tersebut pada ayat (2) tidak dapat dijadikan alasan oleh PIHAK KEDUA untuk menunda pelaksanaan pekerjaan sesuai jadwal yang telah ditetapkan.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel10', array('spaceAfter' => 25, 'align' => 'both'));
        
        $section->addTextBreak(1);

        $section->addText("PASAL 10","secondHeaderFontStyle","headerParagraphStyle");
        $section->addText("LAIN - LAIN","secondHeaderFontStyle","headerParagraphStyle");
        
        $section->addTextBreak(1);

        $section->addText("Hal-hal yang ada hubungannya dengan Surat Perjanjian Kerjasama ini dan belum cukup diatur dalam pasal-pasal Surat Perjanjian Kerjasama ini akan ditentukan Iebih lanjut oleh kedua belah pihak secara musyawarah dan mufakat dalam surat perjanjian tambahan/addendum dan merupakan bagian yang tidak terpisahkan dari perjanjian ini.","contentFontStyle","contentParagraphStyle");

        $section->addTextBreak(1);

        $section->addText("PASAL 11","secondHeaderFontStyle","headerParagraphStyle");
        $section->addText("MASA BERLAKU PERJANJIAN","secondHeaderFontStyle","headerParagraphStyle");
        
        $section->addTextBreak(1);
        
        $section->addListItem("Masa berlaku perjanjian ini adalah selama PIHAK PERTAMA menggunakan aplikasi PIHAK KEDUA.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel11', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Jika PIHAK PERTAMA memutuskan tidak melanjutkan kerjasama dalam penggunaan Aplikasi PIHAK KEDUA, maka PIHAK KEDUA wajib membuka akses kepada PIHAK PERTAMA untuk melakukan migrasi data data secara keseluruhan yang sudah diinput sebelumnya di dalam Aplikasi.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel11', array('spaceAfter' => 25, 'align' => 'both'));

        $section->addTextBreak(1);

        $section->addText("PASAL 12","secondHeaderFontStyle","headerParagraphStyle");
        $section->addText("INFORMASI RAHASIA","secondHeaderFontStyle","headerParagraphStyle");
        
        $section->addTextBreak(1);
        
        $section->addListItem("HAK CIPTA Aplikasi, termasuk didalamnya Source Code, konsep, Copyright adalah milik PIHAK KEDUA.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel12', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Selama berlakunya perjanjian ini maka PIHAK KEDUA berkewajiban untuk menjaga informasi rahasia PIHAK PERTAMA mencakup namun tidak terbatas pada  Ide; Konsep; spesifikasi; Laporan harian; Dokumentasi; Metode; Diagram Strategi pemasaran; Informasi keuangan; Rencana Kerja PIHAK PERTAMA",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel12', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Informasi Rahasia dapat berbentuk, namun tidak terbatas pada, seluruh data data anggota, laporan keuangan dan laporan laporan usaha lainnya yang telah dimasukan di dalam Aplikasi dan dapat diakses oleh PIHAK KEDUA",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel12', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("PIHAK KEDUA tidak akan, tanpa persetujuan tertulis terlebih dahulu dari PIHAK PERTAMA, baik secara langsung atau tidak langsung, lisan atapun tertulis:",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel12', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Membeberkan, melaporkan, menyebarluaskan, menstransfer, membocorkan Informasi Rahasia kepada siapapun juga",1, array('name' => 'Times New Roman', 'size' => 12), 'multilevel12', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Menggunakan Informasi Rahasia untuk tujuan apapun dan dengan cara apapun, yang dapat merugikan PIHAK PERTAMA",1, array('name' => 'Times New Roman', 'size' => 12), 'multilevel12', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("PIHAK PERTAMA dan PIHAK KEDUA mengerti bahwa segala INFORMASI RAHASIA yang telah terdokumentasikan di dalam Aplikasi adalah dan akan tetap menjadi hak milik penuh PIHAK PERTAMA dan wajib untuk dihapus dan dikembalikan kepada PIHAK PERTAMA pada saat berakhirnya Perjanjian ini.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel12', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("PIHAK KEDUA tidak diperbolehkan untuk menyimpan salinan apapun juga dan dalam bentuk apapun juga dari INFORMASI RAHASIA yang telah terdokumentasikan tersebut.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel12', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Segala bentuk pelanggaran terhadap penyebaran informasi rahasia di atas, PIHAK PERTAMA dan PIHAK KEDUA sepakat akan mengacu kepada Undang Undang dan peraturan Hak Cipta yang berlaku di Indonesia.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel12', array('spaceAfter' => 25, 'align' => 'both'));
        
        $section->addTextBreak(1);

        $section->addText("PASAL 13","secondHeaderFontStyle","headerParagraphStyle");
        $section->addText("PENUTUP","secondHeaderFontStyle","headerParagraphStyle");
        
        $section->addTextBreak(1);
        
        $section->addListItem("Surat Perjanjian Kerjasama ini dinyatakan sah dan mengikat kedua belah pihak, serta mulai berlaku sejak tanggal ditanda tangani Surat Perjanjian Kerjasama ini.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel13', array('spaceAfter' => 25, 'align' => 'both'));
        $section->addListItem("Surat Perjanjian Kerjasama ini dibuat dalam rangkap 2 (dua) asli, bermaterai Rp. 6.000,- (Enam ribu rupiah) untuk masing-masing pihak dan mempunyai kekuatan hukum yang sama.",0, array('name' => 'Times New Roman', 'size' => 12), 'multilevel13', array('spaceAfter' => 25, 'align' => 'both'));

        $section->addTextBreak(1);        

        $table3 = $section->addTable($tableStylePihakKedua);
        $table3->addRow(50);
        $table3->addCell(2200)->addText("PIHAK KEDUA", array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'center'));
        $table3->addCell(200);
        $table3->addCell(2200)->addText("PIHAK PERTAMA", array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'center'));
        $table3->addRow(50);
        $table3->addCell(2200);
        $table3->addCell(200);
        $table3->addCell(2200);
        $table3->addRow(50);
        $table3->addCell(2200);
        $table3->addCell(200);
        $table3->addCell(2200);
        $table3->addRow(50);
        $table3->addCell(2200);
        $table3->addCell(200);
        $table3->addCell(2200);
        $table3->addRow(50);
        $table3->addCell(2200);
        $table3->addCell(200);
        $table3->addCell(2200);
        $table3->addRow(50);
        $table3->addCell(2200)->addText("Muhammad Ihsan Firdaus",array('bold' => true, 'underline' => 'single', 'size'=>12, 'name' => 'Times New Roman'),array('align' => 'center'));
        $table3->addCell(200);
        $table3->addCell(2200)->addText("$koperasi->nama_ketua",array('bold' => true, 'underline' => 'single', 'size'=>12, 'name' => 'Times New Roman'),array('align' => 'center'));
        $table3->addRow(50);
        $table3->addCell(2200)->addText("Direktur Utama", array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'center'));
        $table3->addCell(200);
        $table3->addCell(2200)->addText("Ketua Koperasi", array('size'=>12, 'name' => 'Times New Roman'),array('align' => 'center'));

        $objectWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        
        try {
            $objectWriter->save(public_path("/spk_word/SPK_".$koperasi->nama_institusi.".docx"));
        } catch (\Exception $e)
        {
            return back()->with('danger', $e->getMessage());
        }

        return response()->download(public_path("/spk_word/SPK_".$koperasi->nama_institusi.".docx"));
                
    }
    public function exportAllKoperasi()
    {
        return Excel::download(new KoperasiExport, 'ListAllKoperasi.xlsx');
    }
}