<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\KoperasiModel;
use DB;
class KoperasiExport implements FromCollection, WithHeadings
{
    public function collection()
    {
        return DB::table('koperasi')->join('jenis_koperasi', 'jenis_koperasi.id','=','koperasi.jenis_id')->join('provinsi','provinsi.id', '=', 'koperasi.provinsi_id')->join('kabupaten', 'kabupaten.id', '=', 'koperasi.kabupaten_id')->select(['koperasi.id as id','nama_institusi','jenis_koperasi.nama as jenis', 'kabupaten.name as nama_kabupaten','alamat','jml_anggota','tgl_setup'])->get();
    }

    public function headings(): array
    {
        return [
            '#',
            'Nama Koperasi',
            'Jenis Koperasi',
            'Kab/Kota',
            'Alamat',
            'Jumlah Anggota',
            'Tanggal Create'
        ];
    }
}
