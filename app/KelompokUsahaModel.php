<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KelompokUsahaModel extends Model
{
    protected $table = 'kelompok_usaha';

    public function koperasi()
    {
        return $this->hasMany('App\KoperasiModel','kelompok_usaha_id');        
    }
}
