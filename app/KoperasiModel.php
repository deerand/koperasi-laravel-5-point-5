<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class KoperasiModel extends Model
{
    use HasRoles;

    protected $guard_name = 'web'; // or whatever guard you want to use
    protected $table = 'koperasi';
    protected $fillable = [
        'jenis_id', 'users_id', 'sektor_usaha_id', 'bentuk_usaha_id', 'kelompok_usaha_id', 'jenis_usaha_id', 'provinsi_id', 'kabupaten_id', 'kecamatan_id', 'kelurahan_id', 'nomor_spk', 'tgl_spk', 'nama_institusi', 'nama_ketua', 'alamat', 'jml_anggota', 'provinsi', 'kab', 'biaya_hosting', 'pic_teknik', 'no_telp', 'email', 'url', 'syariah', 'level', 'status', 'tgl_setup', 'harga_aplikasi', 'history','status_pembayaran'
    ];

    public function users()
    {
        return $this->belongsTo('App\User');
    }
    public function jenis()
    {
        return $this->belongsTo('App\JenisKoperasiModel');
    }
    public function bentukUsaha()
    {
        return $this->belongsTo('App\BentukUsahaModel');
    }
    public function jenisUsaha()
    {
        return $this->belongsTo('App\JenisUsahaModel');
    }
    public function kelompokUsaha()
    {
        return $this->belongsTo('App\KelompokUsahaModel');
    }
    public function sektorUsaha()
    {
        return $this->belongsTo('App\SektorUsahaModel');
    }
    public function rekap_file()
    {
        return $this->hasMany('App\RekapFileModel','koperasi_id');
    }
    public function provinsi()
    {
        return $this->belongsTo('App\ProvinsiModel');
    }
    public function kabupaten()
    {
        return $this->belongsTo('App\KotaModel');
    }
    public function kecamatan()
    {
        return $this->belongsTo('App\KecamatanModel');
    }
    public function kelurahan()
    {
        return $this->belongsTo('App\KelurahanModel');
    }
}
