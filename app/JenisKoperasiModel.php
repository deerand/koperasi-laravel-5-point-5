<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisKoperasiModel extends Model
{
    protected $table = 'jenis_koperasi';
    protected $fillable = [
        'users_id','nama','image'
    ];

    public function koperasi()
    {
        return $this->hasMany('App\KoperasiModel','jenis_id');        
    }
    public function users()
    {
        return $this->belongsTo('App\User');
    }
}
