<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisUsahaModel extends Model
{
    protected $table = 'jenis_usaha';

    public function koperasi()
    {
        return $this->hasMany('App\KoperasiModel','jenis_usaha_id');        
    }
}
