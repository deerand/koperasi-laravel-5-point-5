<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SektorUsahaModel extends Model
{
    protected $table = 'sektor_usaha';

    public function koperasi()
    {
        return $this->hasMany('App\KoperasiModel','sektor_usaha_id');        
    }
}
