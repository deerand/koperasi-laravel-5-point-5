<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BentukUsahaModel extends Model
{
    protected $table = 'bentuk_usaha';

    public function koperasi()
    {
        return $this->hasMany('App\KoperasiModel','bentuk_usaha_id');        
    }
}
