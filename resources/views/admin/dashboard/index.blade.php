@extends('layouts.app')
@section('title')
    Dashboard
@endsection
@section('headerPage')
    Dashboard
@endsection
@section('isi')
<div class="m-portlet m-portlet--head-lg">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="la la-list"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    Dashboard
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body  m-portlet__body--no-padding">
        <div class="row m-row--no-padding m-row--col-separator-xl">
            <div class="col-sm-12 col-md-6">
                <!--begin:: Widgets/Daily Sales-->
                <div class="m-widget14">
                    <div class="m-widget14__header m--margin-bottom-30">
                        <h3 class="m-widget14__title">
                            Bentuk Usaha
                        </h3>
                        {{-- <span class="m-widget14__desc">
                            Check out each collumn for more details
                        </span> --}}
                    </div>
                    <div class="col" style="margin-bottom: 20px;">
                        <div class="m-widget14__chart" style="height:300px;"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                            <canvas id="bentuk_usaha" width="788" height="600" class="chartjs-render-monitor" style="display: block; width: 394px; height: 300px;"></canvas>
                        </div>
                    </div>
                </div>
                <!--end:: Widgets/Daily Sales-->
            </div>
            <div class="col-sm-12 col-md-6">
                <!--begin:: Widgets/Daily Sales-->
                <div class="m-widget14">
                    <div class="m-widget14__header m--margin-bottom-30">
                        <h3 class="m-widget14__title">
                            Jumlah Koperasi Setiap Kabupaten
                        </h3>
                        {{-- <span class="m-widget14__desc">
                            Check out each collumn for more details
                        </span> --}}
                    </div>
                    <div class="col" style="margin-bottom:20px;">
                        <div class="m-widget14__chart" style="height:300px;"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                            <canvas id="jumlah_tiap_kabupaten" width="788" height="600" class="chartjs-render-monitor" style="display: block; width: 394px; height: 300px;"></canvas>
                        </div>
                    </div>
                </div>
                <!--end:: Widgets/Daily Sales-->
            </div>
            <div class="col-sm-12 col-md-12">
                <!--begin:: Widgets/Daily Sales-->
                <div class="m-widget14">
                    <div class="m-widget14__header m--margin-bottom-30">
                        <h3 class="m-widget14__title">
                            Jumlah Anggota Setiap Koperasi
                        </h3>
                        {{-- <span class="m-widget14__desc">
                            Check out each collumn for more details
                        </span> --}}
                    </div>
                    <div class="col" style="margin-bottom:20px;">
                        <div class="m-widget14__chart" style="height:300px;"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                            <canvas id="jumlah_anggota_tiap_koperasi" width="788" height="600" class="chartjs-render-monitor" style="display: block; width: 394px; height: 300px;"></canvas>
                        </div>
                    </div>
                </div>
                <!--end:: Widgets/Daily Sales-->
            </div>
        </div>
    </div>
</div>
@endsection