@extends('layouts.app')
@section('title')
  Daftar File {{$koperasi->nama_institusi}}
@endsection
@section('isi')
<div class="m-portlet m-portlet--head-lg">
  <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
      <div class="m-portlet__head-title">
        <span class="m-portlet__head-icon">
          <i class="la la-list"></i>
        </span>
        <h3 class="m-portlet__head-text">
          Daftar File {{$koperasi->nama_institusi}}
        </h3>
      </div>
    </div>
    <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
            <li class="m-portlet__nav-item">
                <a href="{{ url('file/create') }}" class="btn m-btn btn-primary btn-sm m-btn--icon m-btn--pill m-btn--air">
                    <span>
                        <i class="la la-plus"></i>
                        <span>Tambah File</span>
                    </span>
                </a>
            </li>
        </ul>
    </div>    
  </div>
  <div class="m-portlet__body">
    @if(session()->has('success'))
      <div class="m-alert m-alert--icon alert m-alert--square alert-success m--margin-bottom-25" role="alert">
        <div class="m-alert__icon">
          <i class="la la-check-circle-o"></i>
        </div>
        <div class="m-alert__text">
          <strong>Berhasil!</strong> {{ session()->get('success') }}
        </div>
        <div class="m-alert__close">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          </button>
        </div>
      </div>
    @endif
    @if(session()->has('danger'))
      <div class="m-alert m-alert--icon alert m-alert--square alert-danger m--margin-bottom-25" role="alert">
        <div class="m-alert__icon">
          <i class="la la-exclamation-circle"></i>
        </div>
        <div class="m-alert__text">
          <strong>Gagal!</strong> {{ session()->get('danger') }}
        </div>
        <div class="m-alert__close">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          </button>
        </div>
      </div>
    @endif
    <div class="list-section">
      @forelse ($koperasi->rekap_file as $item) 
      <div class="list-section__item">
        <div class="section__content image-box">
          <div class="section__desc">
            <h5 class="section__title">{{ $item->nama }}</h5>
            <div class="section__info">
              <div class="section__info__item sm-text">
                <span class="info__label">Posted By :</span>
                <a href="" class="info__detail m-link">{{ $item->users->name }}</a>
              </div>
            </div>
          </div>
        </div>
        <div class="section__action">
          <div class="list__section__action">
            <a href="{{ url('file/'.$item->id.'/edit')}}" class="btn m-btn btn-success btn-sm m-btn--icon m-btn--air icon-only">
              <span>
                <i class="la la-pencil"></i>
                <span>Edit File</span>
              </span>
            </a>
            <a href="javascript:void(0);" onclick="$(this).find('form').submit();" class="btn m-btn btn-outline-danger btn-sm  m-btn--icon m-btn--pill icon-only m_sweetalert_5">
              <span>
                <i class="la la-trash"></i>
                <span>Delete File</span>                
              </span>
              <form action="{{ route('fileKoperasi.destroy', $item->id) }}" method="post">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
              </form>
            </a>
          </div>
        </div>
      </div>
      @empty
      <div class="m-portlet__body">
        <p>Kosong</p>
      </div>        
      @endforelse            
    </div>
  </div>
</div>
@endsection