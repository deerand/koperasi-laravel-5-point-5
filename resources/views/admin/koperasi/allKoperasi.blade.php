@extends('layouts.app')
@section('title')
  Daftar Semua Koperasi
@endsection
@section('isi')
<div class="m-portlet m-portlet--head-lg">
  <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
      <div class="m-portlet__head-title">
        <span class="m-portlet__head-icon">
          <i class="la la-list"></i>
        </span>
        <h3 class="m-portlet__head-text">
          Daftar Semua Koperasi
        </h3>
      </div>
    </div>
    <div class="m-portlet__head-tools">
      <ul class="m-portlet__nav">
        <li class="m-portlet__nav-item">
            <a href="{{ url('file/create') }}" class="btn m-btn btn-primary btn-sm m-btn--icon m-btn--pill m-btn--air">
                <span>
                    <i class="la la-plus"></i>
                    <span>Tambah File</span>
                </span>
            </a>
        </li>
        <li class="m-portlet__nav-item">
          <a href="{{ url('koperasi/create') }}" class="btn m-btn btn-success btn-sm m-btn--icon m-btn--pill m-btn--air">
            <span>
                <i class="la la-plus"></i>
                <span>Tambah Koperasi</span>
            </span>
          </a>
        </li>
        <li class="m-portlet__nav-item">
          <a href="{{ url('exportAllKoperasi') }}" class="btn m-btn btn-warning btn-sm m-btn--icon m-btn--pill m-btn--air">
            <span>
                <i class="la la-plus"></i>
                <span>Export All Koperasi</span>
            </span>
          </a>
        </li>
        
      </ul>
    </div>    
  </div>
  <div class="m-portlet__body no-pedding">
    @if(session()->has('success'))
      <div class="m-alert m-alert--icon alert m-alert--square alert-success m--margin-bottom-25" role="alert">
        <div class="m-alert__icon">
          <i class="la la-check-circle-o"></i>
        </div>
        <div class="m-alert__text">
          <strong>Berhasil!</strong> {{ session()->get('success') }}
        </div>
        <div class="m-alert__close">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          </button>
        </div>
      </div>
    @endif
    @if(session()->has('danger'))
      <div class="m-alert m-alert--icon alert m-alert--square alert-danger m--margin-bottom-25" role="alert">
        <div class="m-alert__icon">
          <i class="la la-exclamation-circle"></i>
        </div>
        <div class="m-alert__text">
          <strong>Gagal!</strong> {{ session()->get('danger') }}
        </div>
        <div class="m-alert__close">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          </button>
        </div>
      </div>
    @endif
    <div class="list-section">
      <div class="m-portlet__body">
      <div class="kt-section__content">
        <table id="all-cop" class="table table-striped- table-bordered table-hover" style="width:100%;">
          <thead>
            <tr>
              <th>Nama Koperasi</th>
              <th>Jenis Koperasi</th>
              <th>Provinsi</th>
              <th>Kabupaten</th>
              <th>Jumlah Anggota</th>
              <th>Tanggal SPK</th>
              <th>Action</th>
            </tr>
          </thead>
        </table>
      </div>
      </div>      
    </div>
  </div>
</div>
@endsection