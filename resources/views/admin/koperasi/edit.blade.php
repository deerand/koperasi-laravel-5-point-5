@extends('layouts.app')
@section('title')
    Edit Koperasi
@endsection
@section('headerPage')
    Edit Koperasi
@endsection
@section('isi')
@if ($errors->any())    
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(session()->has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        {{ session()->get('success') }}
</div>
@endif
@if(session()->has('danger'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        {{ session()->get('danger') }}
</div>
@endif
<form method="POST" action="{{route('koperasi.update',$koperasi->id)}}" enctype="multipart/form-data">
    {{ csrf_field() }}
    {{ method_field('put') }}
        <div class="form-group m-form__group">
            <label for="Nama">Provinsi</label>
            <div class="m-form__control">
                <select class="form-control m-select2 m_select2_4" name="provinsi_id">
                    <option value="">- Pilih Provinsi -</option>
                    @foreach ($provinsi as $item)
                        <option value="{{$item->id}}" {{ $item->id == $koperasi->provinsi->id ? "selected" : "" }}>{{ $item->name }}</option>
                    @endforeach
                </select>
            </div>
            <span class="m-form__help">Tolong Pilih dan Sesuaikan</span>
        </div>
        <div class="form-group m-form__group">
            <label for="Nama">Kota</label>
            <div class="m-form__control">
                <select class="form-control m-select2 m_select2_4" name="kabupaten_id" id="kabupaten_id">
                    @if($koperasi->kabupaten == null)
                        <option value="">- Pilih Kota/Kabupaten</option>
                        @foreach ($kabupaten as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    @else
                        @foreach ($kabupaten as $item)
                            <option value="{{$item->id}}" {{ $item->id == $koperasi->kabupaten->id ? "selected" : "" }}>{{$item->name}}</option>
                        @endforeach                        
                    @endif
                </select>
            </div>
            <span class="m-form__help">Tolong Pilih dan Sesuaikan</span>
        </div>
        <div class="form-group m-form__group">
            <label for="Nama">Kecamatan</label>
            <div class="m-form__control">
                <select class="form-control m-select2 m_select2_4" name="kecamatan_id" id="kecamatan_id">
                    @if($koperasi->kecamatan == null)
                        <option value="">- Pilih Kota/Kabupaten</option>
                        @foreach ($kecamatan as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    @else
                        @foreach ($kecamatan as $item)
                            <option value="{{$item->id}}" {{ $item->id == $koperasi->kecamatan->id ? "selected" : "" }}>{{$item->name}}</option>
                        @endforeach                        
                    @endif
                </select>
            </div>
            <span class="m-form__help">Tolong Pilih dan Sesuaikan</span>
        </div>
        <div class="form-group m-form__group">
            <label for="Nama">Kelurahan</label>
            <div class="m-form__control">
                <select class="form-control m-select2 m_select2_4" name="kelurahan_id">
                    @if($koperasi->kelurahan == null)
                        <option value="">- Pilih Kota/Kabupaten</option>
                        @foreach ($kelurahan as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    @else
                        @foreach ($kelurahan as $item)
                            <option value="{{$item->id}}" {{ $item->id == $koperasi->kelurahan->id ? "selected" : "" }}>{{$item->name}}</option>
                        @endforeach                        
                    @endif
                </select>
            </div>
            <span class="m-form__help">Tolong Pilih dan Sesuaikan</span>
        </div>    
        <div class="form-group">
            <label>Nama Koperasi</label>
            <input type="name" class="form-control" name="nama_institusi" placeholder="Masukan Nama Institusi" value="{{ $koperasi->nama_institusi }}">
        </div>
        <div class="form-group">
            <label>Nomor Badan Hukum Awal</label>
            <input type="name" class="form-control" name="nomor_badan_hukum_awal" placeholder="Masukan Nomor Badan Hukum Awal">
        </div>
        <div class="form-group">
            <label>Nomor Badan Hukum Akhir</label>
            <input type="name" class="form-control" name="nomor_badan_hukum_akhir" placeholder="Masukan Nomor Badan Hukum Akhir">
        </div>
        <div class="form-group">
            <label class="form-control-label">Tanggal SPK</label>                
            <div class="input-group m-input-group">
                <input type="text" name="tgl_spk" class="form-control" id="m_datepicker_1" readonly="" placeholder="Select date &amp; time" value="{{ $koperasi->tgl_spk }}">
                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-calendar-check-o"></i></span></div>
            </div>
        </div>
        <div class="form-group">
            <label for="">Alamat</label>
            <textarea name="alamat" cols="30" rows="10" class="form-control" placeholder="Masukan Alamat">{{ $koperasi->alamat }}</textarea>
        </div>
        <div class="form-group">
            <label>Nomor Telepon PIC Teknik</label>
            <input type="name" class="form-control" name="no_telp" placeholder="Masukan Nomor Telepon" value="{{ $koperasi->no_telp }}">
        </div>
        <div class="form-group">
            <label>Email PIC Teknik</label>
            <input type="email" class="form-control" name="email" placeholder="Masukan Email" value="{{ $koperasi->email }}">
        </div>
        <div class="form-group">
            <label>Website</label>
            <input type="url" class="form-control" name="url" placeholder="Masukan URL" value="{{ $koperasi->url }}">
        </div>
        <div class="form-group m-form__group">
            <label for="Nama">Sektor Usaha</label>
            <div class="m-form__control">
                <select class="form-control m-select2 m_select2_4" name="sektor_usaha_id">
                    @foreach ($sektorUsahaModel as $item)                        
                        <option value="{{ $item->id }}" {{ $item->id == $koperasi->sektor_usaha_id ? "selected" : "" }}>{{ $item->nama }}</option>
                    @endforeach
                </select>
            </div>
            <span class="m-form__help">Tolong Pilih dan Sesuaikan</span>
        </div>
        <div class="form-group m-form__group">
            <label for="Nama">Bentuk Usaha</label>
            <div class="m-form__control">
                <select class="form-control m-select2 m_select2_4" name="bentuk_usaha_id">                    
                    @foreach ($bentukUsahaModel as $item)                        
                        <option value="{{ $item->id }}" {{ $item->id == $koperasi->bentuk_usaha_id ? "selected" : "" }}>{{ $item->nama }}</option>
                    @endforeach
                </select>
            </div>
            <span class="m-form__help">Tolong Pilih dan Sesuaikan</span>
        </div>
        <div class="form-group m-form__group">
            <label for="Nama">Kelompok Usaha</label>
            <div class="m-form__control">
                <select class="form-control m-select2 m_select2_4" name="kelompok_usaha_id">
                    @foreach ($kelompokUsahaModel as $item)                        
                        <option value="{{ $item->id }}" {{ $item->id == $koperasi->kelompok_usaha_id ? "selected" : "" }}>{{ $item->nama }}</option>
                    @endforeach
                </select>
            </div>
            <span class="m-form__help">Tolong Pilih dan Sesuaikan</span>
        </div>
        <div class="form-group m-form__group">
            <label for="Nama">Jenis Usaha</label>
            <div class="m-form__control">
                <select class="form-control m-select2 m_select2_4" name="jenis_usaha_id">
                    @foreach ($jenisUsahaModel as $item)                        
                        <option value="{{ $item->id }}" {{ $item->id == $koperasi->jenis_usaha_id ? "selected" : "" }}>{{ $item->nama }}</option>
                    @endforeach
                </select>
            </div>
            <span class="m-form__help">Tolong Pilih dan Sesuaikan</span>
        </div>
        <div class="form-group">
            <label>Jenis Koperasi</label>
            <select name="jenis_id" class="form-control select2 m_select2_4">
                @foreach ($jenis as $item)
                <option value="{{$item->id}}" {{ $item->id == $koperasi->jenis->id ? "selected" : "" }}>{{$item->nama}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>Nomor SPK</label>
            <input type="name" class="form-control" name="nomor_spk" placeholder="Masukan Nomor SPK" value="{{ $koperasi->nomor_spk }}">
        </div>
        <div class="form-group">
            <label>Nama Ketua</label>
            <input type="name" class="form-control" name="nama_ketua" placeholder="Masukan Nama Ketua" value="{{ $koperasi->nama_ketua }}">
        </div>        
        <div class="form-group">
            <label>Jumlah Anggota</label>
            <input type="number" class="form-control" name="jml_anggota" placeholder="Masukan Jumlah Anggota" value="{{ $koperasi->jml_anggota }}">
        </div>
        <div class="form-group">
            <label>Biaya Hosting</label>
            <input type="number" class="form-control" name="biaya_hosting" placeholder="Masukan Biaya Hosting" value="{{ $koperasi->biaya_hosting }}">
        </div>
        <div class="form-group">
            <label>Nama Pic Teknik</label>
            <input type="text" class="form-control" name="pic_teknik" placeholder="Masukan Pic Teknik" value="{{ $koperasi->pic_teknik }}">
        </div>
        <div class="form-group">
            <label>Syariah</label>
            <select name="syariah" class="form-control select2 m_select2_4">
                @if($koperasi->syariah == "Tidak")
                    <option value="Tidak">Tidak</option>    
                @else                    
                    <option value="Ya">Ya</option>
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>Level</label>
            <select name="level" class="form-control select2 m_select2_4">
                @if($koperasi->level == "Pusat")            
                    <option value="Pusat">Pusat</option>    
                @else                    
                    <option value="Cabang">Cabang</option>
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>Status</label>
            <select name="status" class="form-control select2 m_select2_4">                                
                @if($koperasi->status == "Tidak Aktif")
                    <option value="Tidak Aktif">Tidak Aktif</option>    
                @else                    
                    <option value="Aktif">Aktif</option>
                @endif
            </select>
        </div>        
        <div class="form-group">
            <label class="form-control-label">Tanggal Setup</label>
            <div class="input-group m-input-group">
                <input type="text" name="tgl_setup" class="form-control" id="m_datepicker_1" readonly="" placeholder="Select date &amp; time" value="{{ $koperasi->tgl_setup }}">
                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-calendar-check-o"></i></span></div>
            </div>
        </div>
        <div class="form-group">
            <label>Harga Aplikasi</label>
            <input type="number" class="form-control" name="harga_aplikasi" placeholder="Masukan Harga Aplikasi" value="{{ $koperasi->harga_aplikasi }}">
        </div>
        <div class="form-group">
            <label>History</label>
            <input type="text" class="form-control" name="history" placeholder="Masukan History" value="{{ $koperasi->history }}">
        </div>
        <div class="form-group">
            <label>Status Pembayaran</label>
            <input type="text" class="form-control" name="status_pembayaran" placeholder="Masukan Status Pembayaran" value="{{ $koperasi->status_pembayaran }}">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>        

@endsection