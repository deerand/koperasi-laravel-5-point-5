@extends('layouts.app')
@section('title')
    Tambah Koperasi
@endsection
@section('headerPage')
    Tambah Koperasi
@endsection
@section('isi')
@if ($errors->any())    
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(session()->has('success'))
<div class="m-alert m-alert--icon alert m-alert--square alert-success m--margin-bottom-25" role="alert">
    <div class="m-alert__icon">
        <i class="la la-check-circle-o"></i>
    </div>
    <div class="m-alert__text">
        <strong>Berhasil!</strong> {{ session()->get('success') }}
    </div>
    <div class="m-alert__close">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        </button>
    </div>
</div>
@endif
@if(session()->has('danger'))
<div class="m-alert m-alert--icon alert m-alert--square alert-danger m--margin-bottom-25" role="alert">
    <div class="m-alert__icon">
        <i class="la la-exclamation-circle"></i>
    </div>
    <div class="m-alert__text">
        <strong>Gagal!</strong> {{ session()->get('danger') }}
    </div>
    <div class="m-alert__close">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        </button>
    </div>
</div>
@endif
<form method="POST" action="{{route('koperasi.store')}}">
    {{ csrf_field() }}
    <div class="form-group m-form__group">
        <label for="Nama">Provinsi</label>
        <div class="m-form__control">
            <select class="form-control m-select2 m_select2_4" name="provinsi_id">
                <option value="">- Pilih Provinsi -</option>
                @foreach ($provinsi as $item)
                    <option value="{{$item->id}}">{{ $item->name }}</option>
                @endforeach
            </select>
        </div>
        <span class="m-form__help">Tolong Pilih dan Sesuaikan</span>
    </div>
    <div class="form-group m-form__group">
        <label for="Nama">Kota</label>
        <div class="m-form__control">
            <select class="form-control m-select2 m_select2_4" name="kabupaten_id" id="kabupaten_id">
                <option value="">- Pilih Kabupaten -</option>
            </select>
        </div>
        <span class="m-form__help">Tolong Pilih dan Sesuaikan</span>
    </div>
    <div class="form-group m-form__group">
        <label for="Nama">Kecamatan</label>
        <div class="m-form__control">
            <select class="form-control m-select2 m_select2_4" name="kecamatan_id" id="kecamatan_id">
                <option value="">- Pilih Kecamatan -</option>
            </select>
        </div>
        <span class="m-form__help">Tolong Pilih dan Sesuaikan</span>
    </div>
    <div class="form-group m-form__group">
        <label for="Nama">Kelurahan</label>
        <div class="m-form__control">
            <select class="form-control m-select2 m_select2_4" name="kelurahan_id">
                <option value="">- Pilih Kelurahan -</option>
            </select>
        </div>
        <span class="m-form__help">Tolong Pilih dan Sesuaikan</span>
    </div>
    <div class="form-group">
        <label>Nama Koperasi</label>
        <input type="name" class="form-control" name="nama_institusi" placeholder="Masukan Nama Koperasi">
    </div>
    <div class="form-group">
        <label>Nomor Badan Hukum Awal</label>
        <input type="name" class="form-control" name="nomor_badan_hukum_awal" placeholder="Masukan Nomor Badan Hukum Awal">
    </div>
    <div class="form-group">
        <label>Nomor Badan Hukum Akhir</label>
        <input type="name" class="form-control" name="nomor_badan_hukum_akhir" placeholder="Masukan Nomor Badan Hukum Akhir">
    </div>
    <div class="form-group">
        <label class="form-control-label">Tanggal SPK</label>                
        <div class="input-group m-input-group">
            <input type="text" name="tgl_spk" class="form-control" id="m_datepicker_1" readonly="" placeholder="Select date &amp; time">
            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-calendar-check-o"></i></span></div>
        </div>
    </div>
    <div class="form-group">
        <label for="">Alamat</label>
        <textarea name="alamat" cols="30" rows="10" class="form-control" placeholder="Masukan Alamat"></textarea>
    </div>
    <div class="form-group">
        <label>Nomor Telepon PIC Teknik</label>
        <input type="number" class="form-control" name="no_telp" placeholder="Masukan Nomor Telepon">
    </div>
    <div class="form-group">
        <label>Email PIC Teknik</label>
        <input type="email" class="form-control" name="email" placeholder="Masukan Email">        
    </div>
    <div class="form-group">
        <label>Website</label>
        <input type="url" class="form-control" name="url" placeholder="Masukan URL">
        <span class="m-form__help">Contoh : https://koperasidomain.com</span>
    </div>
    <div class="form-group m-form__group">
        <label for="Nama">Sektor Usaha</label>
        <div class="m-form__control">
            <select class="form-control m-select2 m_select2_4" name="sektor_usaha_id">
                    <option value="">- Pilih Sektor Usaha -</option>
                @foreach ($sektorUsahaModel as $item)
                    <option value="{{$item->id}}">{{ $item->nama }}</option>
                @endforeach
            </select>
        </div>
        <span class="m-form__help">Tolong Pilih dan Sesuaikan</span>
    </div>
    <div class="form-group m-form__group">
        <label for="Nama">Bentuk Usaha</label>
        <div class="m-form__control">
            <select class="form-control m-select2 m_select2_4" name="bentuk_usaha_id">
                    <option value="">- Pilih Bentuk Usaha -</option>
                @foreach ($bentukUsahaModel as $item)
                    <option value="{{$item->id}}">{{ $item->nama }}</option>
                @endforeach
            </select>
        </div>
        <span class="m-form__help">Tolong Pilih dan Sesuaikan</span>
    </div>
    <div class="form-group m-form__group">
        <label for="Nama">Kelompok Usaha</label>
        <div class="m-form__control">
            <select class="form-control m-select2 m_select2_4" name="kelompok_usaha_id">
                    <option value="">- Pilih Kelompok Usaha -</option>
                @foreach ($kelompokUsahaModel as $item)
                    <option value="{{$item->id}}">{{ $item->nama }}</option>
                @endforeach
            </select>
        </div>
        <span class="m-form__help">Tolong Pilih dan Sesuaikan</span>
    </div>
    <div class="form-group m-form__group">
        <label for="Nama">Jenis Usaha</label>
        <div class="m-form__control">
            <select class="form-control m-select2 m_select2_4" name="jenis_usaha_id">
                    <option value="">- Pilih Jenis Usaha -</option>
                @foreach ($jenisUsahaModel as $item)
                    <option value="{{$item->id}}">{{ $item->nama }}</option>
                @endforeach
            </select>
        </div>
        <span class="m-form__help">Tolong Pilih dan Sesuaikan</span>
    </div>
    <div class="form-group">
        <label>Jenis Koperasi</label>
        <select name="jenis_id" class="form-control select2 m_select2_4">                
            @foreach ($jenisKoperasi as $item)
            <option value="{{$item->id}}">{{$item->nama}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Nomor SPK</label>
        <input type="name" class="form-control" name="nomor_spk" placeholder="Masukan Nomor SPK">
    </div>
    <div class="form-group">
        <label>Nama Ketua</label>
        <input type="name" class="form-control" name="nama_ketua" placeholder="Masukan Nama Ketua">
    </div>        
    <div class="form-group">
        <label>Jumlah Anggota</label>
        <input type="number" class="form-control" name="jml_anggota" placeholder="Masukan Jumlah Anggota">
    </div>
    <div class="form-group">
        <label>Biaya Hosting</label>
        <input type="number" class="form-control" name="biaya_hosting" placeholder="Masukan Biaya Hosting">
    </div>
    <div class="form-group">
        <label>Nama Pic Teknik</label>
        <input type="text" class="form-control" name="pic_teknik" placeholder="Masukan Pic Teknik">
    </div>
    <div class="form-group">
        <label>Syariah</label>
        <select name="syariah" class="form-control select2 m_select2_4">                                
            <option value="Tidak">Tidak</option>
            <option value="Ya">Ya</option>
        </select>
    </div>
    <div class="form-group">
        <label>Level</label>
        <select name="level" class="form-control select2 m_select2_4">                                
            <option value="Pusat">Pusat</option>
            <option value="Cabang">Cabang</option>
        </select>
    </div>
    <div class="form-group">
        <label>Status</label>
        <select name="status" class="form-control select2 m_select2_4">                                
            <option value="Tidak Aktif">Tidak Aktif</option>
            <option value="Aktif">Aktif</option>
        </select>
    </div>
    <div class="form-group">
        <label class="form-control-label">Tanggal Setup</label>
        <div class="input-group m-input-group">
            <input type="text" name="tgl_setup" class="form-control" id="m_datepicker_1" readonly="" placeholder="Select date &amp; time">
            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-calendar-check-o"></i></span></div>
        </div>
    </div>
    <div class="form-group">
        <label>Harga Aplikasi</label>
        <input type="number" class="form-control" name="harga_aplikasi" placeholder="Masukan Harga Aplikasi">
    </div>
    <div class="form-group">
        <label>History</label>
        <input type="text" class="form-control" name="history" placeholder="Masukan History">
    </div>
    <div class="form-group">
        <label>Status Pembayaran</label>
        <input type="text" class="form-control" name="status_pembayaran" placeholder="Masukan Status Pembayaran">
    </div>
    <button type="submit" class="btn btn-primary">Simpan</button>
</form>        

@endsection