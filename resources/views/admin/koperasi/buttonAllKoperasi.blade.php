<td>
  <div class="section__action">
    <div class="list__section__action">
  <a href="#" class="btn m-btn btn-primary btn-sm m-btn--icon m-btn--pill m-btn--air icon-only">
  <span>
    <i class="la la-eye"></i>
    <span>Profil Koperasi</span>
  </span>
  </a>
  <a href="javascript:void(0);" onclick="$(this).find('form').submit();" class="btn m-btn btn-outline-danger btn-sm  m-btn--icon m-btn--pill icon-only m_sweetalert_5" title="Delete Koperasi">
    <span>
      <i class="la la-trash"></i>
      <span>Delete Koperasi</span>                
    </span>
    <form action="{{ route('koperasi.destroy', $item->id) }}" method="post">
      {{ csrf_field() }}
      {{ method_field('DELETE') }}
    </form>
  </a>
  <a href="{{ url('koperasi/'.$item->id.'/edit')}}" class="btn m-btn btn-success btn-sm m-btn--icon m-btn--pill icon-only" title="Edit Koperasi">
    <span>
      <i class="la la-pencil"></i>
      <span>Edit Koperasi</span>
    </span>
  </a>                  
  <a href="{{ url('file/'.$item->id) }}" class="btn m-btn btn-info btn-sm m-btn--icon m-btn--pill m-btn--air icon-only m--margin-top-5" title="Management File">
    <span>
      <i class="la la-file"></i>
      <span>Management File</span>
    </span>
  </a>
  <a href="{{ url('export_bap_word/'.$item->id) }}" class="btn m-btn btn-warning btn-sm m-btn--icon m-btn--pill m-btn--air icon-only m--margin-top-5" title="Export Word BAP">
    <span>
        <i class="la la-print"></i>
      <span>Export BAP</span>
    </span>
  </a>
  <a href="{{ url('form_spk/'.$item->id) }}" class="btn m-btn btn-secondary btn-sm m-btn--icon m-btn--pill m-btn--air icon-only m--margin-top-5" title="Export Word SPK">
    <span>
        <i class="la la-print"></i>
      <span>Export SPK</span>
    </span>
  </a>
</td>
