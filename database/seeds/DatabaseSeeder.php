<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use App\User;
use Illuminate\Support\Facades\Hash;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

            User::create([
                'name' => 'Muhamad Ihsan Firdaus',
                'username' => 'ihsanfirdaus',
                'email' => 'ihsanfirdaus@gmail.com',
                'password' => Hash::make('ihsanfirdaus')
            ]);
            // Role::create([
            //     'name' => 'Tamu',
            //     'guard_name' => 'web'
            // ]);
    }
}
