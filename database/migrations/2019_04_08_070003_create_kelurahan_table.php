<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKelurahanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kelurahan', function (Blueprint $table) {
            $table->char('id',10);
            $table->primary('id');
            $table->char('provinsi_id',2);
            $table->foreign('provinsi_id')
            ->references('id')->on('provinsi')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->char('kabupaten_id',4);
            $table->foreign('kabupaten_id')
            ->references('id')->on('kabupaten')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->char('kecamatan_id',7);
            $table->foreign('kecamatan_id')
            ->references('id')->on('kecamatan')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->string('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kelurahan');
    }
}
