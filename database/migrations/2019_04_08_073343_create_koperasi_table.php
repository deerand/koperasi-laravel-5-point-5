<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKoperasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('koperasi', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('jenis_id')->unsigned();
            $table->foreign('jenis_id')
            ->references('id')->on('jenis_koperasi')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->tinyInteger('users_id')->unsigned();
            $table->foreign('users_id')
            ->references('id')->on('users')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->mediumInteger('sektor_usaha_id')->unsigned();
            $table->foreign('sektor_usaha_id')
            ->references('id')->on('sektor_usaha')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->mediumInteger('bentuk_usaha_id')->unsigned();
            $table->foreign('bentuk_usaha_id')
            ->references('id')->on('bentuk_usaha')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->mediumInteger('kelompok_usaha_id')->unsigned();
            $table->foreign('kelompok_usaha_id')
            ->references('id')->on('kelompok_usaha')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->mediumInteger('jenis_usaha_id')->unsigned();
            $table->foreign('jenis_usaha_id')
            ->references('id')->on('jenis_usaha')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->char('provinsi_id',2)->nullable();
            $table->foreign('provinsi_id')
            ->references('id')->on('provinsi')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->char('kabupaten_id',4)->nullable();
            $table->foreign('kabupaten_id')
            ->references('id')->on('kabupaten')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->char('kecamatan_id',7)->nullable();
            $table->foreign('kecamatan_id')
            ->references('id')->on('kecamatan')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->char('kelurahan_id',10)->nullable();
            $table->foreign('kelurahan_id')
            ->references('id')->on('kelurahan')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->string('nomor_spk')->nullable();
            $table->date('tgl_spk')->nullable();
            $table->string('nama_institusi',150);
            $table->string('nomor_badan_hukum_awal', 150)->nullable();
            $table->string('nomor_badan_hukum_akhir', 150)->nullable();
            $table->string('nama_ketua',150)->nullable();
            $table->text('alamat')->nullable();
            $table->mediumInteger('jml_anggota')->nullable();            
            $table->string('pic_teknik')->nullable();
            $table->string('no_telp',16)->nullable();
            $table->string('email',150)->nullable();
            $table->string('url')->nullable();
            $table->string('syariah')->nullable();
            $table->string('level')->nullable();
            $table->string('status')->nullable();
            $table->date('tgl_setup')->nullable();
            $table->integer('harga_aplikasi')->nullable();
            $table->string('history')->nullable();
            $table->string('status_pembayaran')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('koperasi');
    }
}
